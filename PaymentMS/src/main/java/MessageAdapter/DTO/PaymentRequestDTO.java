/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor: Mathias Bramming
 */
package MessageAdapter.DTO;


/**
 * Payment request data transfer object
 *
 * @author Khaled Wafic Khalil and Mathias Bramming
 */
public class PaymentRequestDTO {
    /**
     * Sender of the payment
     */
    private String from;
    /**
     * Receiver of the payment
     */
    private String to;
    /**
     * Amount of money to be transfered
     */
    private int amount;
    /**
     * Description of the transfer
     */
    private String description;
    /**
     * ID of the token used for transfer
     */
    private String tokenId;

    /**
     * Default constructor
     */
    public PaymentRequestDTO() {}

    /**
     *
     * @param from Sender of the payment
     * @param to Receiver of the payment
     * @param amount Amount of money to be transfered
     * @param description Description of the transfer
     * @param tokenId ID of the token to be used for the transfer
     */
    public PaymentRequestDTO(String from, String to, int amount, String description, String tokenId)
    {
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.description = description;
        this.tokenId = tokenId;
    }

    /**
     *
     * @return
     */
    public String getFrom() {
        return from;
    }

    /**
     *
     * @return
     */
    public String getTo() {
        return to;
    }

    /**
     *
     * @return
     */
    public int getAmount() {
        return amount;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @return
     */
    public String getTokenId() {
        return tokenId;
    }

    /**
     *
     * @param tokenId
     */
    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }
}
