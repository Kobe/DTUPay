/*
 **  Primary Contributor:   Mathias Bramming
 **  Secondary Contributor: Karol Marso
 */
package MessageAdapter;

import PaymentCore.InvalidToken;
import PaymentCore.Payment;
import PaymentCore.Payment;
import PaymentCore.PaymentManager;
import bank.BankServiceException_Exception;
import com.google.gson.Gson;
import com.google.gson.Gson;
import com.rabbitmq.client.*;
import org.json.*;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

/**
 * @author Mathias Bramming and Karol Marso
 */
public class MessageReceiver {
    /**
     * Queue for the customer reports
     */
    private String QUEUE_GET_CUSTOMER_REPORTS = "getCustomerReports";
    /**
     * Queue for the merchant reports
     */
    private String QUEUE_GET_MERCHANT_REPORTS = "getMerchantReports";
    /**
     * Queue for requesting the payments
     */
    private String QUEUE_REQUEST_PAYMENT = "requestPayment";
    /**
     * PaymentManager used to retrieve payments
     */
    private PaymentManager paymentManager;
    /**
     * channel used for the communication
     */
    private Channel channel;

    /**
     * Instantiates the Message receiver and makes connection and connects to the channel
     * @param paymentManager PaymentManager used by the microservice
     * @throws IOException
     * @throws TimeoutException
     */
    public MessageReceiver(PaymentManager paymentManager) throws IOException, TimeoutException {
        this.paymentManager = paymentManager;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        Connection connection = factory.newConnection();
        this.channel = connection.createChannel();
    }

    /**
     * Initializes queues
     * @throws IOException
     */
    public void initialize() throws IOException {
        initializeQueue(QUEUE_REQUEST_PAYMENT, this::requestPayment);
        initializeQueue(QUEUE_GET_CUSTOMER_REPORTS, this::callGetCustomerReports);
        initializeQueue(QUEUE_GET_MERCHANT_REPORTS, this::callGetMerchantReports);
    }

    /**
     * Initializes a queue
     *
     * @param queueName Name of the queue to be initialized
     * @param func Function to be called by a queue
     * @throws IOException
     */
    private void initializeQueue(String queueName, Function<String, String> func) throws IOException {
        channel.queueDeclare(queueName, false, false, false, null);
        System.out.println(" [*] Waiting for messages on queue '" + queueName + "'.");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "' on queue '" + queueName +"'");

            AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                    .Builder()
                    .correlationId(delivery.getProperties().getCorrelationId())
                    .build();

            String response = func.apply(message);
            channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, response.getBytes("UTF-8"));
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println("Sent reply for message received on queue '" + queueName + "'.");

        };
        channel.basicConsume(queueName, false, deliverCallback, consumerTag -> { });
    }

    /**
     * Retrieves customer reports
     *
     * @param message Message received on the channel - should be a JSON object with following parameters Date <b>from</b>, Date <b>until</b>, String <b>customerID</b>
     * @return A message consisting of the ArrayList of Payment objects in JSON format
     */
    public String callGetCustomerReports(String message) {
        System.out.println(message);

        JSONObject jsonObject = new JSONObject(message);

        DateFormat formatter = new SimpleDateFormat("MM/dd/yy");

        try {
            Date from = formatter.parse(jsonObject.getString("from"));
            Date until = formatter.parse(jsonObject.getString("until"));
            ArrayList<Payment> payments= paymentManager.getCustomerPayments(from, until, (String)jsonObject.get("customerID"));

            return new Gson().toJson(payments);
        } catch (ParseException e) {
            e.printStackTrace();

            return null;
        }


    }

    /**
     * Retrieves merchant reports
     * @param message Message received on the channel - should be a JSON object with following parameters Date <b>from</b>, Date <b>until</b>, String <b>customerID</b>
     * @return A message consisting of the ArrayList of Payment objects in JSON format
     */
    private String callGetMerchantReports(String message) {
        System.out.println(message);

        JSONObject jsonObject = new JSONObject(message);

        DateFormat formatter = new SimpleDateFormat("MM/dd/yy");

        try {
            Date from = formatter.parse(jsonObject.getString("from"));
            Date until = formatter.parse(jsonObject.getString("until"));
            ArrayList<Payment> payments = paymentManager.getMerchantPayments(from, until, (String) jsonObject.get("merchantID"));


            return new Gson().toJson(payments);
        } catch (ParseException e) {
            e.printStackTrace();

            return null;
        }
    }

    /**
     * Requests payment to be performed
     *
     * @param arg1 Message containing important payment details
     * @return "PaymentSuccess" message if correct otherwise "BankError" or "InvalidToken" message
     */
    public String requestPayment(String arg1) {
        Gson gson = new Gson();
        Payment dto = gson.fromJson(arg1, Payment.class);
        try {
            paymentManager.MakeTransaction(dto);
            return "PaymentSuccess";
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return "BankError";
        } catch (InvalidToken invalidToken) {
            invalidToken.printStackTrace();
            return "InvalidToken";
        }
    }
}
