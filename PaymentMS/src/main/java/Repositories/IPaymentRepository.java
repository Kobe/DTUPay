/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor:
 */
package Repositories;


import PaymentCore.Payment;


import java.util.ArrayList;
import java.util.Date;

/**
 * @author Khaled Wafic Khalil
 * Interface for the Payment repository
 */
public interface IPaymentRepository {

    /**
     *
     * @param p is the payment
     * @return the status of adding the payment (true || false)
     */
    boolean add(Payment p);

    /**
     *
     * @param p is the payment
     * @return the status of removing the payment (true || false)
     */
    boolean remove(Payment p);

    /**
     *
     * @param from start date
     * @param until end date
     * @param customerID the customer cpr
     * @return all the payments related to the customer
     */
    public ArrayList<Payment> getPaymentsCustomer(Date from, Date until, String customerID);

    /**
     *
     * @param from start date
     * @param until end date
     * @param merchantID the merchant id
     * @return all the payments related to the merchant
     */
    public ArrayList<Payment> getPaymentsMerchant(Date from, Date until, String merchantID);
}
