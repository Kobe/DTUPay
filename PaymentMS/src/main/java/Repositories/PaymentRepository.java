/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor: Karol Marso
 */
package Repositories;

import PaymentCore.Payment;
import Repositories.IPaymentRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Khaled Wafic Khalil
 */
public class PaymentRepository implements IPaymentRepository {

    /**
     * Contains all payments, token id is the key
     */
    private HashMap<String, Payment> _payments;

    public PaymentRepository() {
        this._payments = new HashMap<>();
    }

    /**
     *
     * @param p is the payment
     * @return status of adding the payment (true || false)
     */
    @Override
    public boolean add(Payment p) {

        //No token found
        if (p.getTokenId() == null || p.getTokenId().isEmpty()){
            System.out.println("NO token found");
            return false;
        }

        //Token already used
        if(_payments.containsKey(p.getTokenId())) {
            System.out.println("Token already used");
            return false;
        }

        //Add payment
        _payments.put(p.getTokenId(), p);
        return true;
    }

    /**
     *
     * @param p is the payment
     * @return status of removing the payment (true || false)
     */
    @Override
    public boolean remove(Payment p) {
        return _payments.remove(p.getTokenId(), p);
    }


    /**
     *
     * @param from start date
     * @param until end date
     * @param customerID the customer cpr
     * @return all payments related to the customer
     */
    @Override
    public ArrayList<Payment> getPaymentsCustomer(Date from, Date until, String customerID) {
        ArrayList<Payment> payments = new ArrayList<Payment>();

        for (Map.Entry<String, Payment> payment:
             _payments.entrySet()) {
            if (payment.getValue().getFrom().equals(customerID) && payment.getValue().getDate().before(until) && payment.getValue().getDate().after(from)){
                payments.add(payment.getValue());
            }
        }

        return payments;
    }

    /**
     *
     * @param from start date
     * @param until end date
     * @param merchantID the merchant id
     * @return all payments related to the merchant
     */
    @Override
    public ArrayList<Payment> getPaymentsMerchant(Date from, Date until, String merchantID) {
        ArrayList<Payment> payments = new ArrayList<Payment>();

        for (Map.Entry<String, Payment> payment:
                _payments.entrySet()) {
            if (payment.getValue().getTo().equals(merchantID) && payment.getValue().getDate().before(until) && payment.getValue().getDate().after(from)){
                payments.add(payment.getValue());
            }
        }

        return payments;
    }
}
