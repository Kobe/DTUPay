/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor:
 */
package PaymentCore;

import Repositories.IPaymentRepository;
import Repositories.PaymentRepository;
import bank.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Khaled Wafic Khalil
 * This class manage everythin with payments.
 */
public class PaymentManager {

    /**
     * The bank interface imported from wsdl
     */
    private BankService bankService;
    /**
     * The payment interface
     */
    private IPaymentRepository _repo;

    /**
     *
     * @param repository
     */
    public PaymentManager(PaymentRepository repository)  {

        this.bankService = new BankServiceService().getBankServicePort();
        this._repo = repository;
    }

    /**
     *
     * @param payment
     * @throws BankServiceException_Exception
     * @throws InvalidToken
     */
    public void MakeTransaction(Payment payment) throws BankServiceException_Exception, InvalidToken {

        if (_repo.add(payment))
        {
            Account from = bankService.getAccountByCprNumber(payment.getFrom());
            Account to = bankService.getAccountByCprNumber(payment.getTo());


            String debtor = from.getId();
            String creditor = to.getId();

            bankService.transferMoneyFromTo(debtor, creditor, new BigDecimal(payment.getAmount()), payment.getDescription());
        }
        else{
            throw new InvalidToken("Token is already used.");
        }
    }

    /**
     *
     * @param from
     * @param until
     * @param customerID
     * @return all payments related to the customer CPR
     */
    public ArrayList<Payment> getCustomerPayments(Date from, Date until, String customerID){
        return this._repo.getPaymentsCustomer(from, until, customerID);
    }

    /**
     *
     * @param from
     * @param until
     * @param merchantID
     * @return all payments related to the merchant ID
     */
    public ArrayList<Payment> getMerchantPayments(Date from, Date until, String merchantID){
        return this._repo.getPaymentsMerchant(from, until, merchantID);
    }
}
