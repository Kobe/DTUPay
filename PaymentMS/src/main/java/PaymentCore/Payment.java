/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor:
 */
package PaymentCore;


import java.util.Date;

/**
 * @author Khaled Wafic Khalil
 * This class represent a payment
 */
public class Payment {

    /**
     * Debtor
     */
    private String from;
    /**
     * Creditor
     */
    private String to;
    /**
     * The amount to be transfered
     */
    private int amount;
    /**
     * Payment description
     */
    private String description;
    /**
     * The id of the token
     */
    private String tokenId;
    /**
     * Transaction date
     */
    private Date date;


    /**
     * Empty constructer
     */
    public Payment() {
    }

    /**
     *
     * @param from debtor
     * @param to creditor
     * @param amount amount to be transfered
     * @param description description of the payment
     * @param tokenId the id of the token
     */
    public Payment(String from, String to, int amount, String description, String tokenId)
    {
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.description = description;
        this.tokenId = tokenId;
        this.date = new Date();
    }

    /**
     *
     * @return debor CPR
     */
    public String getFrom() {
        return from;
    }

    /**
     *
     * @return creditor ID
     */
    public String getTo() {
        return to;
    }

    /**
     *
     * @return amount
     */
    public int getAmount() {
        return amount;
    }

    /**
     *
     * @return Description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @return token id
     */
    public String getTokenId() {
        return tokenId;
    }

    /**
     *
     * @param tokenId
     */
    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    /**
     *
     * @return the date
     */
    public Date getDate(){
        return this.date;
    }
}
