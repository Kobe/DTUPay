/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor:
 */
package PaymentCore;

/**
 * This class is used for identify duplicated of payments. For instance if a customer tries to use
 * an already used token to finish a payment, this exception will be thrown.
 *
 * @author Khaled Wafic Khalil
 */
public class InvalidToken extends Throwable {
    public InvalidToken(String message) { super((message));}
}

