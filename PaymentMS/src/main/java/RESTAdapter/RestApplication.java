/*
 **  Primary Contributor:   Mathias Bramming
 **  Secondary Contributor:
 */
package RESTAdapter;

import MessageAdapter.MessageReceiver;
import PaymentCore.PaymentManager;
import Repositories.PaymentRepository;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Main class of the microservice
 *
 * @author Mathias Bramming
 */
@ApplicationPath("/")
public class RestApplication extends Application {
    /**
     * PaymentManager used by the microservice
     */
    private PaymentManager paymentManager = null;

    /**
     * Instantiates important classes such as PaymentManager and MessageReceiver
     */
    public RestApplication() {
        System.out.println("Initializing application...");
        paymentManager = new PaymentManager(new PaymentRepository());
        MessageReceiver receiver = null;
        try {
            receiver = new MessageReceiver(paymentManager);
            receiver.initialize();
        } catch (Exception e) {
            System.out.println("Failed to initialize message queue.");
            e.printStackTrace();
        }
    }

    /**
     * Retrieves the singletons in the class - StatusResoruce and PaymentResource
     *
     * @return Set of singletons
     */
    @Override
    public Set<Object> getSingletons() {
        Set<Object> set = new HashSet<>();
        set.add(new StatusResource());
        set.add(new PaymentResource(paymentManager));
        return set;
    }

}
