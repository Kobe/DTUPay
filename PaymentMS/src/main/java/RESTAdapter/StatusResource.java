/*
 **  Primary Contributor:   Mathias Bramming
 **  Secondary Contributor:
 */
package RESTAdapter;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * Used for the testing purposes - shows the status of the service
 * @author Mathias Bramming
 */
@Path("/status")
public class StatusResource {
    /**
     *
     * @return
     */
    @GET
    @Produces("text/plain")
    public Response doGet() {
        return Response.ok("Payment service is up.").build();
    }
}