/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor:
 */
package RESTAdapter;

import PaymentCore.InvalidToken;
import PaymentCore.Payment;
import PaymentCore.PaymentManager;
import bank.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Main service of the PaymentMS microservice used mostly for testing purposes, but can be potentially accessed to directly perform payments
 *
 * @author Khaled Wafic Khalil
 */
@Path("/payment")
public class PaymentResource {

    /**
     * PaymentManager used by the microservice
     */
    private PaymentManager paymentManager;

    /**
     * Constructs the service
     *
     * @param paymentManager PaymentManager used by the microservice
     */
    public PaymentResource(PaymentManager paymentManager) {
        this.paymentManager = paymentManager;
    }

    /**
     * Transfers money
     *
     * @param payment Payment details
     * @return 200 OK if the payment was successful otherwise returns 400
     * @throws InvalidToken
     */
    @Path("/transfer")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response TransferMoney(Payment payment) throws InvalidToken {
        try {

            paymentManager.MakeTransaction(payment);
            return Response.status(Response.Status.OK).entity("Transaction went successfully").build();

        } catch (BankServiceException_Exception e)
        {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getFaultInfo().getErrorMessage()).build();
        }

    }

}
