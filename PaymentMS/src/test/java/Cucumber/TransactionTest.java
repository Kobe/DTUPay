/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor: Mathias Bramming
 */
package Cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "Features")
public class TransactionTest {
}
