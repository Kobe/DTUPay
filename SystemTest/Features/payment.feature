#   Primary contributor: Bilal
#   Secondary contributor(s):

Feature: Payment
  Scenario: Simple payment
    Given a registered customer with a bank account
    And a registered merchant with a bank account
    And the customer has at least one unused token
    And the token is retrievable by the customer
    When the merchant scans the token and requests payment for 100 kroner using the token
    Then the payment succeeds, and the money is transferred from the customer bank account to the merchant bank account
    And the used token is invalidated