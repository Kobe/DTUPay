/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor: Bilal El Ghoul
 */


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "Features")
public class PaymentTest {
}
