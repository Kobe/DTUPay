/*
 **  Primary Contributor:   Bilal El Ghoul
 **  Secondary Contributor: Khaled Wafic Khalil, Mathias Bramming
 */


import bank.BankService;
import bank.BankServiceException_Exception;
import bank.BankServiceService;
import bank.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.json.JSONArray;

import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;


public class PaymentSteps {

    private String merchantCPR;
    private String merchantFirstName;
    private String merchantLastName;

    private String customerCPR;
    private String customerFirstName;
    private String customerLastName;

    private final String host = "http://02267-kobe2.compute.dtu.dk";
    //private final String host = "http://192.168.99.100"; //for local runs
    private final String CustomerPort = ":8080";
    private final String MerhcantPort = ":8081";
    private final String TokenPort = ":8082";
    private final String ReportPort = ":8083";
    private final String PaymentPort = ":8085";
    private ArrayList<TokenResponse> customerTokens = new ArrayList<TokenResponse>();


    @Before
    public void Setup()

    {
        Random rnd = new Random();
        int mCPR = rnd.nextInt(999999999) + 1000000000;
        int cCPR = rnd.nextInt(999999999) + 1000000000;

        this.merchantCPR = Integer.toString(mCPR);
        this.merchantFirstName = "Merchant";
        this.merchantLastName = "MerchantLastName";

        this.customerCPR = Integer.toString(cCPR);
        this.customerFirstName = "Customer";
        this.customerLastName = "CustomerLastName";

        System.out.println("Merchant CPR: " + merchantCPR);
        System.out.println("Customer CPR: " + customerCPR);
    }

    @Given("^a registered customer with a bank account$")
    public void a_registered_customer_with_a_bank_account() throws Throwable {


        String url = host + CustomerPort + "/customer/register";
        Client client = Client.create();
        WebResource webResource = client
                .resource(url);

        JsonObject customerJson = new JsonObject();
        customerJson.addProperty("cpr", customerCPR);
        customerJson.addProperty("firstName", customerFirstName);
        customerJson.addProperty("lastName", customerLastName);

        String jsonString = customerJson.toString();

        ClientResponse response = webResource.type("application/json")
                .post(ClientResponse.class, jsonString);

        if (response.getStatus() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }

        assertEquals(200, response.getStatus());

        BankService bankService = new BankServiceService().getBankServicePort();
        try{

            User user;

            //Create a new user
            user = new User();
            user.setCprNumber(customerCPR);
            user.setFirstName(customerFirstName);
            user.setLastName(customerLastName);

            BigDecimal amount = new BigDecimal("200.0");

            //Register the user at the bank
            bankService.createAccountWithBalance(user, amount);
            //System.out.println("BankService message:\n>>");
        }
        catch (BankServiceException_Exception e)
        {
            System.out.println("(Customer)BankService exception:\n>> " + e.getMessage());
        }

    }

    @Given("^a registered merchant with a bank account$")
    public void a_registered_merchant_with_a_bank_account() {

        String url = host + MerhcantPort  + "/merchant/register";


        Client client = Client.create();
        WebResource webResource = client
                .resource(url);

        JsonObject merchantObject = new JsonObject();

        merchantObject.addProperty("id", merchantCPR);
        merchantObject.addProperty("firstName", merchantCPR);
        merchantObject.addProperty("lastName", merchantCPR);

        ClientResponse response = webResource.type("application/json")
                .post(ClientResponse.class, merchantObject.toString());

        if (response.getStatus() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }

        assertEquals(200, response.getStatus());

        BankService bankService = new BankServiceService().getBankServicePort();
        try{

            User user;

            //Create a new user
            user = new User();
            user.setCprNumber(merchantCPR);
            user.setFirstName(merchantFirstName);
            user.setLastName(merchantLastName);

            BigDecimal amount = new BigDecimal("100.0");

            //Register the user at the bank
            bankService.createAccountWithBalance(user, amount);
            //System.out.println("BankService message:\n>>");
        }
        catch (BankServiceException_Exception e)
        {
            System.out.println("(Merchant)BankService exception:\n>> " + e.getMessage());
        }

    }

    @Given("^the customer has at least one unused token$")
    public void the_customer_has_at_lease_one_unused_token() throws Throwable {

        String url = host + TokenPort  + "/tokens/request";

        Client client = Client.create();
        WebResource webResource = client
                .resource(url);

        JsonObject requestObject = new JsonObject();

        requestObject.addProperty("_CPR", customerCPR);
        requestObject.addProperty("_numberOfTokens", 2);

        ClientResponse response = webResource.type("application/json")
                .post(ClientResponse.class, requestObject.toString());

        assertEquals(200, response.getStatus());

        JSONArray jsonArr = new JSONArray(response.getEntity(String.class));

        ObjectMapper objectMapper = new ObjectMapper();

        for (int i = 0; i < jsonArr.length(); i++) {
            customerTokens.add(objectMapper.readValue(jsonArr.get(i).toString(), TokenResponse.class));
        }

    }

    @Given("^the token is retrievable by the customer$")
    public void the_token_is_retrievable_by_the_customer() throws Throwable {
        //String url = host + TokenPort + "/tokens/barcode/";
        //String barcode = customerTokens.get(0).getBarCode();

        String url = customerTokens.get(0).getURL();

        Client client = Client.create();
        WebResource webResource = client
                .resource(url);


        ClientResponse response = webResource.type("application/json")
                .get(ClientResponse.class);

        assertEquals(Response.Status.OK, response.getResponseStatus());
    }

    @When("^the merchant scans the token and requests payment for (\\d+) kroner using the token$")
    public void the_merchant_scans_the_token_and_requests_payment_for_kroner_using_the_token(int arg1) throws Throwable {


        String tokenID = customerTokens.get(0).getBarCode();

        String url = host + MerhcantPort  + "/merchant/transfer/";

        System.out.println("Bar Code/ Token ID: " +  tokenID);

        Client client = Client.create();
        WebResource webResource = client
                .resource(url);

        JsonObject transferRequest = new JsonObject();

        transferRequest.addProperty("merchantID", merchantCPR);
        transferRequest.addProperty("tokenID", tokenID);
        transferRequest.addProperty("amount", arg1);
        transferRequest.addProperty("message", "hohoho");

        ClientResponse response = webResource.type("application/json")
                .post(ClientResponse.class, transferRequest.toString());

        assertEquals(200, response.getStatus());
    }


    @Then("^the payment succeeds, and the money is transferred from the customer bank account to the merchant bank account$")
    public void the_payment_succeeds_and_the_money_is_transferred_from_the_customer_bank_account_to_the_merchant_bank_account() throws Throwable {
        BankService bankService = new BankServiceService().getBankServicePort();

        assertEquals(new BigDecimal("100.0"),bankService.getAccountByCprNumber(customerCPR).getBalance());
        assertEquals(new BigDecimal("200.0"), bankService.getAccountByCprNumber(merchantCPR).getBalance());
    }

    @Then("^the used token is invalidated$")
    public void the_used_token_is_invalidated() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        String tokenID = customerTokens.get(0).getBarCode();

        String url = host + MerhcantPort  + "/merchant/transfer/";

        Client client = Client.create();
        WebResource webResource = client
                .resource(url);

        JsonObject transferRequest = new JsonObject();

        transferRequest.addProperty("merchantID", merchantCPR);
        transferRequest.addProperty("tokenID", tokenID);
        transferRequest.addProperty("amount", 100);
        transferRequest.addProperty("message", "hohoho");

        ClientResponse response = webResource.type("application/json")
                .post(ClientResponse.class, transferRequest.toString());

        assertEquals(Response.Status.NOT_ACCEPTABLE, response.getResponseStatus());

    }

}
