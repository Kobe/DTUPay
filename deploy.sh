#   Primary contributor:        Mathias Bramming
#   Secondary contributor(s):

#!/usr/bin/env sh
scriptdir="$(dirname "$0")"
cd "$scriptdir"
set -e
GREEN="\033[1;32m"
NOCOLOR="\033[0m"

log_console()
{
    echo -e "${GREEN}$1${NOCOLOR}"
}

wait_service_rdy()
{
    until docker-compose logs | grep "$1"
    do
        sleep 2s
    done
}

# start
log_console "Building and deploying DTUPay...\n"
log_console "Cleaning builds ('mvn clean')...\n"
mvn clean

# package
log_console "\nPackaging microservices ('mvn package')...\n"
mvn package

# docker
log_console "\nShutting down running Docker containers ('docker-compose down')...\n"
docker-compose down
log_console "\nBuilding and starting Docker containers ('docker-compose up -d --build --force-recreate')...\n"
docker-compose up -d --build --force-recreate

# wait for container initialization
log_console "\nWaiting for containers to initialize...\n"
wait_service_rdy 'Application rabbitmq_management started on node rabbit'
wait_service_rdy 'Deployed "CustomerMS'
wait_service_rdy 'Deployed "MerchantMS'
wait_service_rdy 'Deployed "TokenMS'
wait_service_rdy 'Deployed "ReportMS'
wait_service_rdy 'Deployed "PaymentMS'

# run external tests
# TODO: Add external tests here

# run system tests
log_console "\nRunning System Tests...\n"
pushd ./SystemTest
mvn test
popd