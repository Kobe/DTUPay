#   Primary contributor: Bilal El Ghoul
#   Secondary contributor(s):

Feature: Request tokens

    @CucumberTests.requestTokens
    Scenario: Customer requets 1 token
        Given a customer with 1 token
        When the customer requests 1 token
        Then the customer has 2 tokens in total

    @CucumberTests.requestTokens
    Scenario: Customer requests 6 tokens
        Given a customer with 1 unused token
        When the customer requests 6 tokens
        Then the customer 1 token in total