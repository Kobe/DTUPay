/*
 **  Primary Contributor:   Bilal El Ghoul
 **  Secondary Contributor: Mathias Bramming
 */
package TokenCore;

import MessageAdapter.IMessageSender;
import Repositories.ITokenRepository;
import Repositories.TokenRepository;

import java.io.IOException;
import java.util.HashSet;

/**
 * Core logic of the TokenMS microservice
 *
 * @author Bilal El Ghoul and Mathias Bramming
 */
public class TokenManager {
    /**
     * Token Repository
     */
    private ITokenRepository repo;
    /**
     * MessageSender used by the manager
     */
    private IMessageSender messageSender;

    /**
     *
     * @param repo TokenRepository used by the manager
     * @param messageSender MessageSender used by the manager
     */
    public TokenManager(TokenRepository repo, IMessageSender messageSender) {
        this.repo = repo;
        this.messageSender = messageSender;
    }

    /**
     * Requests for the customer
     *
     * @param CPR CPR of the customer
     * @param numberOfTokens Number of requested tokens
     * @return Set of generated tokens
     * @throws InvalidTokenException
     * @throws IOException
     * @throws InterruptedException
     */
    public HashSet<Token> requestTokens(String CPR, int numberOfTokens) throws InvalidTokenException, IOException, InterruptedException {

        if (!isCustomerRegistered(CPR)) {
            throw new InvalidTokenException("Customer is not registered");
        }

        HashSet<Token> customerTokens = repo.getCustomerTokens(CPR);

        if (numberOfTokens < 1 || numberOfTokens > 5) {
            throw new InvalidTokenException("Invalid Number of Requested Tokens");
        } else if (customerTokens == null) {

            for (int i = 0; i < numberOfTokens; i++) {

                repo.addTokens(genToken(), CPR);
            }

        } else if (customerTokens.size() != 1 || (customerTokens.size() + numberOfTokens) > 6) {
            throw new InvalidTokenException("Number of Unused Tokens is Not Zero");
        } else {
            for (int i = 0; i < numberOfTokens; i++) {
                repo.addTokens(genToken(), CPR);
            }
        }

        return repo.getCustomerTokens(CPR);
    }

    /**
     * Uses a token and returns the owner`s ID which is used in payment
     *
     * @param barCode barcode of the token
     * @return ID of the customer to whom the token belongs
     */
    public String useToken(String barCode) {
        String userID = "";
        if (repo.tokenExists(barCode)) {
            if (repo.isTokenValid(barCode)) {
                userID = repo.getCustomer(barCode);
                repo.invalidateToken(barCode);
            }
        }
        System.out.println("returning userID:" + userID);
        return userID;
    }

    /**
     * Generates token
     *
     * @return Generated Token
     * @throws IOException
     */
    public Token genToken() throws IOException {
        Token t = new Token();

        // As long as the repo contains the token, generate another one.
        while (repo.tokenExists(t.getBarCode().getCode())) {
            t = new Token();
        }
        return t;
    }

    /**
     * Retrieves token based on a barcode
     *
     * @param barcode Barcode number
     * @return Retrieved Token
     * @throws InvalidTokenException
     */
    public Token getToken(String barcode) throws InvalidTokenException {
        return repo.getToken(barcode);
    }

    /**
     * Retrieves tokens of the customer
     *
     * @param CPR CPR of the customer
     * @return Set of customer`s tokens
     */
    public HashSet<Token> getCustomerTokens(String CPR)
    {
        return repo.getCustomerTokens(CPR);
    }

    /**
     * Checks if the customer is registered
     *
     * @param CPR CPR of the registered customer
     * @return true if the customer is registered otherwise false
     * @throws IOException
     * @throws InterruptedException
     */
    private boolean isCustomerRegistered(String CPR) throws IOException, InterruptedException {
        return messageSender.callCheckCustomer(CPR);
    }

    /**
     * Inserts token to the database
     *
     * @param t Token to be inserted
     * @param CPR CPR of the owner of the token
     */
    public void addTokens(Token t, String CPR) {
        this.repo.addTokens(t, CPR);
    }

    /**
     * Clears the database
     */
    public void clearDatabase() {
        this.repo.clearDatabase();
    }

}
