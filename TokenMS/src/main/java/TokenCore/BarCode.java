/*
 **  Primary Contributor:   Bilal El Ghoul
 **  Secondary Contributor:
 */
package TokenCore;

import org.krysalis.barcode4j.impl.upcean.EAN13Bean;
import org.krysalis.barcode4j.impl.upcean.UPCEANLogicImpl;

import java.util.Random;

/**
 * Barcode class
 *
 * @author Bilal El Ghoul
 */
public class BarCode {

    /**
     * Barcode bean
     */
    private EAN13Bean _bean;

    /**
     * Code of the barcode
     */
    private String code;

    /**
     * Generates Barcode
     */
    public BarCode() {
        this.code = genEAN13Code();
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Generates barcode bean
     *
     * @return barcode code
     */
    private String genEAN13Code() {

        Random rand = new Random();
        _bean = new EAN13Bean();
        UPCEANLogicImpl impl = _bean.createLogicImpl();

        String code = "";

        for (int i = 0; i < 12; i++) {
            code += Integer.toString(rand.nextInt(9) + 1);
        }

        code += impl.calcChecksum(code);

        return code;
    }

    /**
     *
     * @return
     */
    public EAN13Bean getBean() {
        return this._bean;
    }
}
