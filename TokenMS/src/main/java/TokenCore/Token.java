/*
 **  Primary Contributor:   Bilal El Ghoul
 **  Secondary Contributor:
 */
package TokenCore;

import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Token of used in the microservice
 *
 * @author Bilal El Ghoul
 */
public class Token {
    /**
     * specifies if the token was used
     */
    private boolean _isUsed;
    /**
     * url of the barcode
     */
    private String _url;
    /**
     * Barcode code
     */
    private String Barcode;
    /**
     * Barcode fo the token
     */
    private BarCode _barCode;
    /**
     * Image of the token
     */
    private BufferedImage _image;

    public Token() throws IOException {
        this._barCode = new BarCode();
        this._isUsed = false;
        this._url = "";
        this._image = genBarCodeBufferedImage(320);
    }

    public boolean isUsed() {
        return _isUsed;
    }

    public void setUsed() {
        _isUsed = true;
    }

    public String getUrl() {
        return _url;
    }

    public void setUrl(String url) {
        this._url = url;
    }

    public BarCode getBarCode() {
        return this._barCode;
    }

    public BufferedImage getBufferedImage() {
        return this._image;
    }

    /**
     * Retrieves barcode image
     *
     * @param dpi DPI of the screen for the barcode - important when using different screens
     * @return Image of the barcode
     * @throws IOException
     */
    private BufferedImage genBarCodeBufferedImage(int dpi) throws IOException {

        BitmapCanvasProvider provider = new BitmapCanvasProvider(
                dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);

        this._barCode.getBean().generateBarcode(provider, this._barCode.getCode());

        provider.finish();

        return provider.getBufferedImage();
    }

}
