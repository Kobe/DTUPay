/*
 **  Primary Contributor:   Bilal El Ghoul
 **  Secondary Contributor:
 */
package TokenCore;

/**
 * Exception of the invalid token
 *
 * @author Bilal El Ghoul
 */
public class InvalidTokenException extends Throwable {
    public InvalidTokenException(String message) {
        super(message);
    }
}
