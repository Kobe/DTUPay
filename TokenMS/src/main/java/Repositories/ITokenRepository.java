/*
 **  Primary Contributor:   Bilal El Ghoul
 **  Secondary Contributor: Khaled Wafic Khalil
 */
package Repositories;

import TokenCore.InvalidTokenException;
import TokenCore.Token;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;

/**
 * Token repository interface
 *
 * @author Bilal El Ghoul and Khaled Wafic Khalil
 */
public interface ITokenRepository {
    /**
     * Adds token to the repository
     *
     * @param token Token to be added
     * @param CPR CPR of the owner
     * @return
     */
    boolean addTokens(Token token, String CPR);

    /**
     * Retrieves tokens for a customer
     *
     * @param CPR CPR of the customer
     * @return Set of tokens owned by customer
     */
    HashSet<Token> getCustomerTokens(String CPR);

    /**
     * Retrieves token from barcode
     *
     * @param barCode Barcode of the token
     * @return Token instance that is of the barcode
     * @throws InvalidTokenException
     */
    Token getToken(String barCode) throws InvalidTokenException;

    /**
     * Invalidates a token based on the barcode
     *
     * @param barcode Barcode of the token
     * @return true if the token was used otherwise false
     */
    boolean invalidateToken(String barcode);

    /**
     * Checks if the token exists based on barcode
     *
     * @param barCode Barcode of the token
     * @return true if token exists else false
     */
    boolean tokenExists(String barCode);

    /**
     * Checks if the token is valid based on Barcode
     *
     * @param barcode Barcode of the token
     * @return true if token exists otherwise false
     */
    boolean isTokenValid(String barcode);

    /**
     * Retrieves customer ID based on the barcode
     * @param barCode Barcode of the token
     * @return CustomerID (CPR)
     */
    String getCustomer(String barCode);

    /**
     * Clears the database
     *
     * @return true if database was successfully cleared otherwise returns false
     */
    boolean clearDatabase();
}
