/*
 **  Primary Contributor:   Bilal El Ghoul
 **  Secondary Contributor: Khaled Wafic Khalil
 */
package Repositories;

import TokenCore.InvalidTokenException;
import TokenCore.Token;

import java.util.*;

/**
 * @author  Bilal El Ghoul and Khaled Wafic Khalil
 */
public class TokenRepository implements ITokenRepository {

    /**
     * Tokens owned by the user in a form CPR-Token
     */
    private LinkedHashMap<String, HashSet<Token>> _tokenOwners;
    /**
     * All tokens in a form Token-TokenID
     */
    private LinkedHashMap<Token, String> _allTokens;

    /**
     *
     */
    public TokenRepository(){
        this._tokenOwners = new LinkedHashMap<String, HashSet<Token>>();
        this._allTokens  = new LinkedHashMap<Token, String>();
    }


    /**
     * Adds token to the repository
     *
     * @param token Token to be added
     * @param CPR CPR of the owner
     * @return true if token was succesfully inserted
     */
    @Override
    public boolean addTokens(Token token, String CPR) {

        if(!this._tokenOwners.containsKey(CPR)) {

            // if the customer has not requested tokens before.
            this._tokenOwners.put(CPR, new HashSet<Token>());
        }

        // add the token to the customer hashset
        this._tokenOwners.get(CPR).add(token);

        // add the token to the list of all tokens
        this._allTokens.put(token, CPR);

        return true;
    }

    /**
     * Retrieves a token from the repository
     * @param CPR CPR of the customer
     * @return returns set of Customer`s tokens
     */
    @Override
    public HashSet<Token> getCustomerTokens(String CPR) {

        return (HashSet<Token>) this._tokenOwners.get(CPR);
    }

    /**
     * Retrieves a token
     *
     * @param barCode Barcode of the token
     * @return Token object
     * @throws InvalidTokenException
     */
    @Override
    public Token getToken(String barCode) throws InvalidTokenException {
        if(tokenExists(barCode)) {
            for (Token token : _allTokens.keySet()) {
                if (token.getBarCode().getCode().equals(barCode)) {
                    return token;
                }
            }
        } else {
            throw new InvalidTokenException("No Token Found");

        }
        return null;
    }


    /**
     * Invalidates a token
     *
     * @param barCode Barcode of the token
     * @return true if the token was succesfully invalidated
     */
    @Override
    public boolean invalidateToken(String barCode) {

        // Iterate over all tokens
        for (Token t : _allTokens.keySet()) {

            // if the barCode matches the barCode within the token
            if(t.getBarCode().getCode().equals(barCode)) {

                t.setUsed();

                // Get the CPR associated with this token
                String CPR = _allTokens.get(t);

                // Remove the token from _tokenOwners.
                _tokenOwners.get(CPR).remove(t);

                return true;
            }
        }

        return false;

    }

    /**
     * Checks if the token exists
     *
     * @param barCode Barcode of the token
     * @return true if the token exists else false
     */
    @Override
    public boolean tokenExists(String barCode) {

        for (Token t : _allTokens.keySet()) {
            if(t.getBarCode().getCode().equals(barCode)) return true;
        }

        return false;
    }

    /**
     * Retrieves owner of the token
     *
     * @param barCode Barcode of the token
     * @return ID of the token owner
     */
    @Override
    public String getCustomer(String barCode) {

        String CPR = null;
        if(tokenExists(barCode)) {
            for (Token token : _allTokens.keySet()) {
                if(token.getBarCode().getCode().equals(barCode)) {
                    if(!token.isUsed()) {
                        CPR =  _allTokens.get(token);
                    }
                }
            }
        }

        return CPR;
    }

    /**
     * Checks if the token was used
     * @param barCode Barcode of the token
     * @return true if the token was used else false
     */
    @Override
    public boolean isTokenValid(String barCode) {
        boolean state = false;

        for (Token t : _allTokens.keySet()) {
            if(t.getBarCode().getCode().equals(barCode)) {
                state = !t.isUsed();
            }
        }
        return state;
    }

    /**
     * Clears database
     * @return
     */
    @Override
    public boolean clearDatabase() {
        this._tokenOwners = new LinkedHashMap<String, HashSet<Token>>();
        this._allTokens  = new LinkedHashMap<Token, String>();
        return true;
    }
}
