/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor: Mathias Bramming
 */
package MessageAdapter;

import java.io.IOException;

/**
 * Interface for the MessageSender adapter of the TokenMS microservice
 * @author Karol Marso and Mathias Bramming
 */
public interface IMessageSender {
    /**
     * Checks if the customer exists in the microservice
     *
     * @param CPR CPR of the customer
     * @return true if the customer exists else returns false
     * @throws IOException
     * @throws InterruptedException
     */
    boolean callCheckCustomer(String CPR) throws IOException, InterruptedException;
}
