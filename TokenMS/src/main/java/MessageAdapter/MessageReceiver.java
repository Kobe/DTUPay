/*
 **  Primary Contributor:   Mathias Bramming
 **  Secondary Contributor:
 */
package MessageAdapter;

import TokenCore.TokenManager;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

/**
 * MessageReceiver of the TokenMS microservice
 *
 * @author Mathias Bramming
 */
public class MessageReceiver {
    /**
     * Queue for checking the tokens
     */
    private String QUEUE_CHECK_TOKEN = "checkToken";
    /**
     * TokenManager used by the microservice
     */
    private TokenManager tokenManager;
    /**
     * Channel used for communication
     */
    private Channel channel;

    /**
     * Instantiates MessageReceiver - makes connection and connects to the channel
     *
     * @param tokenManager TokenManager used by the microservice
     * @throws IOException
     * @throws TimeoutException
     */
    public MessageReceiver(TokenManager tokenManager) throws IOException, TimeoutException {
        this.tokenManager = tokenManager;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        Connection connection = factory.newConnection();
        this.channel = connection.createChannel();
    }

    /**
     * Initializes queues
     * @throws IOException
     */
    public void initialize() throws IOException {
        initializeQueue(QUEUE_CHECK_TOKEN, this::callCheckToken);
    }

    /**
     * Initializes a queue
     * @param queueName Name of the queue to be connected to
     * @param func Function performed on reception of message
     * @throws IOException
     */
    private void initializeQueue(String queueName, Function<String, String> func) throws IOException {
        channel.queueDeclare(queueName, false, false, false, null);
        System.out.println(" [*] Waiting for messages on queue '" + queueName + "'.");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "' on queue '" + queueName +"'");

            AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                    .Builder()
                    .correlationId(delivery.getProperties().getCorrelationId())
                    .build();

            String response = func.apply(message);
            channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, response.getBytes("UTF-8"));
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println("Sent reply for message received on queue '" + queueName + "'.");

        };
        channel.basicConsume(queueName, false, deliverCallback, consumerTag -> { });
    }

    /**
     * Checks if the token was used
     * @param tokenID ID of the token to be checked
     * @return CPR of the customer if the token was not used yet
     */
    public String callCheckToken(String tokenID) {
        return tokenManager.useToken(tokenID); // user CPR if token was unused
    }
}
