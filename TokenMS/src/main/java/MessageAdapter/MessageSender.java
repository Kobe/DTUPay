/*
 **  Primary Contributor:   Mathias Bramming
 **  Secondary Contributor: Karol Marso
 */
package MessageAdapter;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;

/**
 * Message sender for the TokenMS microservice
 * @author Mathias Bramming and Karol Marso
 */
public class MessageSender implements IMessageSender {
    /**
     * Connection where sender should connect to
     */
    private Connection connection;
    /**
     * Channel the sender should get connected to
     */
    private Channel channel;
    /**
     * Queue for the customer checking
     */
    private String QUEUE_CHECK_CUSTOMER = "checkCustomer";

    /**
     * Instantiates MessageSender - makes connection and creates a channel
     *
     * @throws IOException
     * @throws TimeoutException
     */
    public MessageSender() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        System.out.println("Sender instantiated");
        connection = factory.newConnection();
        channel = connection.createChannel();
    }


    /**
     * Sends message to the queue
     *
     * @param queueName Name of the queue where the message should be sent to
     * @param message Message to be sent
     * @return Received message
     * @throws IOException
     * @throws InterruptedException
     */
    private String call(String queueName, String message) throws IOException, InterruptedException {
        final String corrId = UUID.randomUUID().toString();
        System.out.println("Sending request: " + message);

        String replyQueueName = channel.queueDeclare().getQueue();
        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();

        channel.basicPublish("", queueName, props, message.getBytes("UTF-8"));

        System.out.println("Sent message...");

        final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);

        System.out.println("Waiting for reply...");
        String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                response.offer(new String(delivery.getBody(), "UTF-8"));
            }
        }, consumerTag -> {
        });

        String result = response.take();
        System.out.println("Received message: " + result);
        channel.basicCancel(ctag);
        return result;
    }

    /**
     * Calls function to check the customer in another microservice
     *
     * @param CPR CPR of the customer
     * @return true if the customer exists otherwise returns false
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public boolean callCheckCustomer(String CPR) throws IOException, InterruptedException {
        boolean exists = Boolean.parseBoolean(call(this.QUEUE_CHECK_CUSTOMER, CPR));
        return exists;
    }
}
