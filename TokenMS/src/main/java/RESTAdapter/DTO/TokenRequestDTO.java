/*
 **  Primary Contributor:   Bilal El Ghoul
 **  Secondary Contributor:
 */
package RESTAdapter.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Data transfer object to request token
 *
 * @author Bilal El Ghoul
 */
public class TokenRequestDTO {
    /**
     * Number of tokens to be requested
     */
    @JsonProperty("_numberOfTokens")
    private int _numberOfTokens;
    /**
     * CPR of the token owner
     */
    @JsonProperty("_CPR")
    private String _CPR;

    public TokenRequestDTO(){

    }

    public String get_CPR() {
        return this._CPR;
    }

    public void set_CPR(String _CPR) {
        this._CPR = _CPR;
    }

    public int get_numberOfTokens() {
        return _numberOfTokens;
    }

    public void set_numberOfTokens(int _numberOfTokens) {
        this._numberOfTokens = _numberOfTokens;
    }
}
