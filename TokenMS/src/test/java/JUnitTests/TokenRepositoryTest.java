/*
 **  Primary Contributor:   Bilal El Ghoul
 **  Secondary Contributor:
 */
package JUnitTests;

import MessageAdapter.IMessageSender;
import MessageAdapter.MessageSender;
import TokenCore.InvalidTokenException;
import TokenCore.TokenManager;
import Repositories.TokenRepository;
import org.junit.Before;
import org.junit.Test;
import TokenCore.Token;

import java.io.IOException;
import java.util.HashSet;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.*;

public class TokenRepositoryTest {

    TokenRepository tr;
    TokenManager tm;
    String CPR;

    public TokenRepositoryTest() {
    }

    @Before
    public void setUp() {
        tr = new TokenRepository();
        tm = new TokenManager(tr, new IMessageSender() {
            @Override
            public boolean callCheckCustomer(String CPR) throws IOException, InterruptedException {
                return true;
            }
        });
        CPR = "12345678950";
    }

    @Test
    public void TokenRepository_addTokens() throws IOException {

        tr.addTokens(new Token(), CPR);
        assertEquals(1, tr.getCustomerTokens(CPR).size());
    }

    @Test
    public void TokenRepository_getCustomerTokens() throws IOException {

        try {
            assertEquals(null, tr.getCustomerTokens(CPR));
        }catch (NullPointerException e) {

        }
        tr.addTokens(new Token(), CPR);
        assertEquals(HashSet.class, tr.getCustomerTokens(CPR).getClass());
    }

    @Test
    public void TokenRepository_getToken() throws IOException, InvalidTokenException {

        Token t = new Token();
        String barCode = t.getBarCode().getCode();
        tr.addTokens(t, CPR);

        assertEquals(t, tr.getToken(barCode));
        assertEquals(barCode, tr.getToken(barCode).getBarCode().getCode());

        //
        try {
            tm.requestTokens(CPR, 5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        HashSet<Token> customerToken = tm.getCustomerTokens(CPR);

        for (Token token : customerToken) {
            assertEquals(token.getBarCode().getCode(), tm.getToken(token.getBarCode().getCode()).getBarCode().getCode());
        }


    }

    @Test
    public void TokenRepository_invalidateToken() throws IOException {
        Token t = new Token();
        String barCode = t.getBarCode().getCode();
        tr.addTokens(t, CPR);

        assertEquals(1, tr.getCustomerTokens(CPR).size());

        tr.invalidateToken(barCode);
        assertEquals(0, tr.getCustomerTokens(CPR).size());
    }

    @Test
    public void TokenRepository_tokenExists() throws IOException {

        Token t = new Token();
        String barCode = t.getBarCode().getCode();
        tr.addTokens(t, CPR);

        assertTrue(tr.tokenExists(barCode));
    }

    @Test
    public void TokenRepository_isTokenValid() throws IOException {

        Token t = new Token();
        String barCode = t.getBarCode().getCode();
        tr.addTokens(t, CPR);

        assertTrue(tr.isTokenValid(barCode));

        tr.invalidateToken(barCode);

        assertFalse(tr.isTokenValid(barCode));

    }
}