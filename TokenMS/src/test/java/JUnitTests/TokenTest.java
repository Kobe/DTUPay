/*
 **  Primary Contributor:   Bilal El Ghoul
 **  Secondary Contributor:
 */
package JUnitTests;

import TokenCore.BarCode;
import TokenCore.Token;
import org.junit.Before;
import org.junit.Test;
import sun.security.pkcs11.wrapper.CK_TOKEN_INFO;

import static org.junit.Assert.*;

public class TokenTest {

    public Token t;

    @Before
    public void setUp() throws Exception {
        t = new Token();
    }

    @Test
    public void Token_isUsed() {
        assertFalse(t.isUsed());
    }

    @Test
    public void Token_setUsed() {
        t.setUsed();
        assertTrue(t.isUsed());
    }

    @Test
    public void Token_getUrl() {
        assertEquals("", t.getUrl());
    }

    @Test
    public void Token_setUrl() {
        t.setUrl("TEST-URL");
        assertEquals("TEST-URL", t.getUrl());
    }

    @Test
    public void Token_getBarCode() {
        assertNotNull(t.getBarCode());
        assertEquals(BarCode.class, t.getBarCode().getClass());
    }

    @Test
    public void Token_getBufferedImage() {
        assertNotNull(t.getBufferedImage());
    }
}