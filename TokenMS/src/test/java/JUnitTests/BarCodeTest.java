/*
 **  Primary Contributor:   Bilal El Ghoul
 **  Secondary Contributor:
 */
package JUnitTests;

import TokenCore.BarCode;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BarCodeTest {


    BarCode bc;

    @Before
    public void setUp() throws Exception {
        bc = new BarCode();
    }

    @Test
    public void BarCode_getCode() {
        assertNotNull(bc.getCode());
        assertEquals(13, bc.getCode().length());
    }

    @Test
    public void BarCode_getBean() {
        assertNotNull(bc.getBean());
    }

}