/*
 **  Primary Contributor:   Bilal El Ghoul
 **  Secondary Contributor:
 */
package CucumberTests.useToken;

import MessageAdapter.IMessageSender;
import TokenCore.Token;
import TokenCore.TokenManager;
import Repositories.TokenRepository;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class TokenManager_useTokenSteps {

    public TokenManager tm = new TokenManager(new TokenRepository(), new IMessageSender() {
        @Override
        public boolean callCheckCustomer(String CPR) throws IOException, InterruptedException {
            return true;
        }
    });

    public String CPR_1 = "1234567890";
    public String barCode = "";


    @Given("^a customer with (\\d+) token$")
    public void a_customer_with_token(int arg1) throws Throwable {

        tm.clearDatabase();

        Token t = new Token();
        barCode = t.getBarCode().getCode();

        tm.addTokens(t, CPR_1);
        assertEquals(arg1, tm.getCustomerTokens(CPR_1).size());
    }

    @When("^the customer uses the token$")
    public void the_customer_uses_the_token() throws Throwable {
        tm.useToken(barCode);
    }

    @Then("^the customer has (\\d+) tokens in total$")
    public void the_customer_has_tokens_in_total(int arg1) throws Throwable {
        assertEquals(arg1, tm.getCustomerTokens(CPR_1).size());
    }

}
