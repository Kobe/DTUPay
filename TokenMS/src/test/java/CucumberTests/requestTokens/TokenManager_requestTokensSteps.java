/*
 **  Primary Contributor:   Bilal El Ghoul
 **  Secondary Contributor:
 */
package CucumberTests.requestTokens;

import MessageAdapter.IMessageSender;
import MessageAdapter.MessageSender;
import TokenCore.InvalidTokenException;
import TokenCore.Token;
import TokenCore.TokenManager;
import Repositories.TokenRepository;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.*;


public class TokenManager_requestTokensSteps {

    public TokenManager tm = new TokenManager(new TokenRepository(), new IMessageSender(){
        @Override
        public boolean callCheckCustomer(String CPR){
            return true;
        }

    });

    public String CPR_1 = "1234567890";
    public String CPR_2 = "1234567899";

    public TokenManager_requestTokensSteps() throws IOException, TimeoutException {
    }

    @Given("^a customer with (\\d+) token$")
    public void a_customer_with_token(int arg1) throws Throwable {
            tm.addTokens(new Token(), CPR_1);
            assertEquals(arg1, tm.getCustomerTokens(CPR_1).size());
    }

    @When("^the customer requests (\\d+) token$")
    public void the_customer_requests_token(int arg1) throws Throwable {
       tm.requestTokens(CPR_1, arg1);
    }

    @Then("^the customer has (\\d+) tokens in total$")
    public void the_customer_has_tokens_in_total(int arg1) throws Throwable {
        assertEquals(arg1, tm.getCustomerTokens(CPR_1).size());
    }

    /*
     *  Customer requests 5 tokens
     */
    @Given("^a customer with (\\d+) unused token$")
    public void a_customer_with_unused_token(int arg1) throws Throwable {
        tm.addTokens(new Token(), CPR_2);
        assertEquals(arg1, tm.getCustomerTokens(CPR_2).size());
    }


    @When("^the customer requests (\\d+) tokens$")
    public void the_customer_requests_tokens(int arg1) throws Throwable {
        try {
            tm.requestTokens(CPR_2, arg1);
        }catch (InvalidTokenException e) {

        }
    }

    @Then("^the customer (\\d+) token in total$")
    public void the_customer_shoudl_not_receive_any_tokens(int arg1) throws Throwable {
        assertEquals(arg1, tm.getCustomerTokens(CPR_2).size());
    }

}
