#   Primary contributor:        Mathias Bramming
#   Secondary contributor(s):

#!/usr/bin/env sh
scriptdir="$(dirname "$0")"
cd "$scriptdir"
set -e
GREEN="\033[1;32m"
NOCOLOR="\033[0m"

log_console()
{
    echo -e "${GREEN}$1${NOCOLOR}"
}

log_console "Building merchantms ('mvn package')...\n"
mvn package

pushd "../"
log_console "\nBuilding Docker image and starting container ('docker-compose up -d --build merchantms')...\n"
docker-compose up -d --build merchantms
popd

log_console "\nContainer started. It might take a while to initialize"