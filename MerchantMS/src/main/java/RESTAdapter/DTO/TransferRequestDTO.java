/*
 **  Primary Contributor:   Mathias Bramming
 **  Secondary Contributor: Bilal Ahmad El Ghhoul
 */
package RESTAdapter.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * TransferRequest Data transfer objec
 *
 * @author Mathias Bramming and Bilal Ahmad El Ghhoul
 */
public class TransferRequestDTO {
    /**
     * Merchant identifier
     */
    @JsonProperty("merchantID")
    private String merchantID;
    /**
     * token identifier
     */
    @JsonProperty("tokenID")
    private String tokenID;
    /**
     * amount of money to be transfered
     */
    @JsonProperty("amount")
    private int amount;
    /**
     * Message inside transfer
     */
    @JsonProperty("message")
    private String message;

    /**
     *
     * @return
     */
    public String getMerchantID() {
        return merchantID;
    }

    /**
     *
     * @param merchantID
     */
    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    /**
     *
     * @return
     */
    public String getTokenID() {
        return tokenID;
    }

    /**
     *
     * @param tokenID
     */
    public void setTokenID(String tokenID) {
        this.tokenID = tokenID;
    }

    /**
     *
     * @return
     */
    public int getAmount() {
        return amount;
    }

    /**
     *
     * @param amount
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     *
     * @return
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
