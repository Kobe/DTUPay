/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor: Mathias Bramming
 */
package RESTAdapter;


import MerchantCore.*;
import RESTAdapter.DTO.TransferRequestDTO;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Main REST API of the MerchantMS
 *
 * @author  Khaled Wafic Khalil and Mathias Bramming
 */
@Path("/merchant")
public class MerchantResource {
    /**
     * MerchantManager used by the microservice
     */
    private MerchantManager merchantManager;

    /**
     * Constructs the MerchantResource
     * @param merchantManager MerchantManager used by the microservice
     */
    public MerchantResource(MerchantManager merchantManager)  {
        this.merchantManager = merchantManager;
    }

    /**
     * Registers the merchant in the microservice
     * @param merchant Merchant to be inserted
     * @return 200 OK if the merchant was successfully added otherwise 400
     */
    @Path("/register/")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerMerchant(Merchant merchant) {
        Merchant m = merchant;

        if (merchantManager.repo.add(merchant))
        {
            return Response.ok(new Gson().toJson("Merchant created!")).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    /**
     * Transfers the money
     *
     * @param dto Data transfer object for the transfer request
     * @return 200 OK if the payment was successful, otherwise 415 if the payment failed
     */
    @Path("/transfer")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response transfer(TransferRequestDTO dto) {

        boolean success = merchantManager.transfer(dto);
        if (success) {
            return Response.status(Response.Status.OK).entity(new Gson().toJson("Payment Successful")).build();
        }
        return Response.status(Response.Status.NOT_ACCEPTABLE).entity(new Gson().toJson("Payment Failed")).build();
    }

}

