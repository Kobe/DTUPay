/*
 **  Primary Contributor:   Mathias Bramming
 **  Secondary Contributor:
 */
package RESTAdapter;

import MerchantCore.MerchantManager;
import MessageAdapter.MessageSender;
import Repositories.MerchantRepository;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeoutException;

/**
 * Instantiates the
 *
 * @author Mathias Bramming
 */
@ApplicationPath("/")
public class RestApplication extends Application {
    /**
     * MerchantManager used by the microservice
     */
    private MerchantManager merchantManager = null;

    /**
     * Instantiates application and important classes such as manager class
     *
     * @throws IOException
     * @throws TimeoutException
     */
    public RestApplication() throws IOException, TimeoutException {
        System.out.println("Initializing application...");
        merchantManager = new MerchantManager(new MerchantRepository(), new MessageSender());
    }

    /**
     * Creates singletons within the class for the StatusResource and for the MerchantResource
     *
     * @return
     */
    @Override
    public Set<Object> getSingletons() {
        Set<Object> set = new HashSet<>();
        set.add(new StatusResource());
        set.add(new MerchantResource(merchantManager));
        return set;
    }
}
