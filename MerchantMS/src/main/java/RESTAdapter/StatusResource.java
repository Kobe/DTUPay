/*
 **  Primary Contributor:   Mathias Bramming
 **  Secondary Contributor:
 */
package RESTAdapter;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * Status resource is used to check the status of the microservice
 *
 * @author Mathias Bramming
 */
@Path("/status")
public class StatusResource {

    /**
     * Checks the status of the microservice
     * @return 200 OK if the microservice is up and running
     */
    @GET
    @Produces("text/plain")
    public Response doGet() {
        return Response.ok("Merchant service is up.").build();
    }
}