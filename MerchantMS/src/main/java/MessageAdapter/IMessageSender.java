/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor: Mathias Bramming
 */
package MessageAdapter;

import MerchantCore.Merchant;

import java.io.IOException;

/**
 * Interface for the communication between other microservices and MerchantMS
 * @author Karol Marso and Mathias Bramming
 */
public interface IMessageSender {
    /**
     * Calls check token to send the message to check the tokens
     * @param tokenId ID of the token used
     * @return true if token is ok, otherwise false
     * @throws IOException
     * @throws InterruptedException
     */
    String callCheckToken(String tokenId) throws IOException, InterruptedException;

    /**
     * Calls request payment message to other microservice responsible for the transfer of the money
     *
     * @param amount Amount of the money to be transfered
     * @param fromAccount Identifier of the payment sender
     * @param toBankAccount Identifier of the payment receiver
     * @param description Description of the payment
     * @param tokenID ID of the used token
     * @return true if the transfer was successful
     */
    boolean callRequestPayment(int amount, String fromAccount, String toBankAccount, String description, String tokenID);

    /**
     * Sends message to add merchant to another microservice
     *
     * @param m Merchant to be added
     * @return true if the Merchant was successfully added
     */
    boolean callAddMerchant(Merchant m);

    /**
     * Sends message to remove merchant from another microservice
     *
     * @param m Merchant to be removed
     * @return true if the removal was successful otherwise false
     */
    boolean callRemoveMerchant(Merchant m);

    /**
     * Sends message to retrieve Merchant from other microservice
     * @param ID ID of the merchant to be retrieved
     * @return Retrieved merchant
     */
    Merchant callGetMerchantByID(String ID);
}
