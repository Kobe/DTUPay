/*
 **  Primary Contributor:   Mathias Bramming
 **  Secondary Contributor: Karol Marso
 */
package MessageAdapter;

import MerchantCore.Merchant;
import MessageAdapter.DTO.PaymentRequestDTO;
import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;

/**
 * @author Mathias Bramming and Karol Marso
 */
public class MessageSender implements IMessageSender {
    /**
     * Connection for the sender
     */
    private Connection connection;
    /**
     * Channel for the communication
     */
    private Channel channel;

    /**
     * Queue used to check the token
     */
    private String QUEUE_CHECK_TOKEN = "checkToken";
    /**
     * Queue to request the payment
     */
    private String QUEUE_REQUEST_PAYMENT = "requestPayment";

    /**
     * Constructs the message sender - connects to connection and channel
     *
     * @throws IOException
     * @throws TimeoutException
     */
    public MessageSender() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        System.out.println("Sender instantiated");
        connection = factory.newConnection();
        channel = connection.createChannel();
    }

    /**
     * Sends the message to a specified queue
     * @param queueName Name of the queue to send the message
     * @param message Parameters for the receiving end
     * @return Message retrieved in the queue
     * @throws IOException
     * @throws InterruptedException
     */
    private String call(String queueName, String message) throws IOException, InterruptedException {
        final String corrId = UUID.randomUUID().toString();
        System.out.println("Sending request: " + message);

        String replyQueueName = channel.queueDeclare().getQueue();
        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();

        channel.basicPublish("", queueName, props, message.getBytes("UTF-8"));

        final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);

        String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                response.offer(new String(delivery.getBody(), "UTF-8"));
            }
        }, consumerTag -> {
        });

        String result = response.take();
        System.out.println("Received message: " + result);
        channel.basicCancel(ctag);
        return result;
    }

    /**
     * Checks the token
     *
     * @param tokenID ID of the token to be checked
     * @return if the token is valid or not
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public String callCheckToken(String tokenID) throws IOException, InterruptedException {
        return call(QUEUE_CHECK_TOKEN, tokenID);
    }

    /**
     * Requests payment from another service
     *
     * @param amount Amount of the money to be transfered
     * @param fromAccount Identifier of the payment sender
     * @param toBankAccount Identifier of the payment receiver
     * @param description Description of the payment
     * @param tokenID ID of the used token
     * @return true if the payment was successful
     */
    @Override
    public boolean callRequestPayment(int amount, String fromAccount, String toBankAccount, String description, String tokenID) {
        System.out.println("Calling requestpayment");
        PaymentRequestDTO dto = new PaymentRequestDTO(fromAccount, toBankAccount, amount, description, tokenID);
        try {
            String result = call(QUEUE_REQUEST_PAYMENT, (new Gson()).toJson(dto));
            System.out.println("result " + result);
            if ("PaymentSuccess".equals(result)) {
                System.out.println("Payment success!");
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Adds merchant to another microservice
     *
     * @param m Merchant to be added
     * @return true if successful addition of the merchant
     */
    @Override
    public boolean callAddMerchant(Merchant m) {
        return false;
    }

    /**
     * Removes the merchant from another microservice
     * @param m Merchant to be removed
     * @return true if the removal was successful
     */
    @Override
    public boolean callRemoveMerchant(Merchant m) {
        return false;
    }

    /**
     * Retrieves merchant from another microservice
     *
     * @param ID ID of the merchant to be retrieved
     * @return Retrieved merchant
     */
    @Override
    public Merchant callGetMerchantByID(String ID) {
        return null;
    }
}
