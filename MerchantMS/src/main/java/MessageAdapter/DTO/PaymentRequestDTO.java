/*
 **  Primary Contributor:   Mathias Bramming
 **  Secondary Contributor: Khaled Wafic Khalil
 */
package MessageAdapter.DTO;

/**
 * Data transfer object for payment request
 *
 * @author Mathias Bramming and Khaled Wafic Khalil
 */
public class PaymentRequestDTO {
    /**
     * Identifier of the payment sender
     */
    private String from;
    /**
     * Identifier of the payment receiver
     */
    private String to;
    /**
     * Amount of money to be transfered
     */
    private int amount;
    /**
     * Description of the transfer
     */
    private String description;
    /**
     * ID of the token used for the transfer
     */
    private String tokenId;

    /**
     * Default constructor
     */
    public PaymentRequestDTO() {}

    /**
     * Constructor of the PaymentRequestDTO data transfer object
     *
     * @param from Identifier of the payment sender
     * @param to Identifier of the payment receiver
     * @param amount Amount of money to be transfered
     * @param description  Description of the transfer
     * @param tokenId ID of the token used for the transfer
     */
    public PaymentRequestDTO(String from, String to, int amount, String description, String tokenId)
    {
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.description = description;
        this.tokenId = tokenId;
    }

    /**
     *
     * @return
     */
    public String getFrom() {
        return from;
    }

    /**
     *
     * @return
     */
    public String getTo() {
        return to;
    }

    /**
     *
     * @return
     */
    public int getAmount() {
        return amount;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @return
     */
    public String getTokenId() {
        return tokenId;
    }

    /**
     *
     * @param tokenId
     */
    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }
}
