/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor:
 */
package Repositories;

import MerchantCore.Merchant;

import java.util.HashMap;

/**
 * Merchant repository of the MerchantMS microservice
 *
 * @author Khaled Wafic Khalil
 */
public class MerchantRepository implements IMerchantRepository {

    /**
     * Stores the merchants in form ID - Name
     */
    private HashMap<String, String> _merchants;


    /**
     * Default constructor
     */
    public MerchantRepository(){
        _merchants = new HashMap<String, String>();
    }

    /**
     * Adds merchant to the repository
     *
     * @param m merchant to be inserted
     * @return true if the merchant was successfully inserted
     */
    @Override
    public boolean add(Merchant m) {
        try{
            _merchants.put(m.getID(), m.getFullName());
            return true;
        }
        catch (Exception e)
        {
            //User already exists
            return false;
        }
    }

    /**
     * Removes merchant from the repository
     *
     * @param m merchant to be removed
     * @return true if the merchant was successfully removed
     */
    @Override
    public boolean remove(Merchant m) {
        return _merchants.remove(m.getID(), m.getFullName());

    }

    /**
     * Retrieves merchant from the repository
     *
     * @param CPR ID of the merchant to be retrieved
     * @return Merchant to be retrieved
     */
    @Override
    public Merchant getMerchantByID(String CPR) {
        String fullName = _merchants.get(CPR);

        if (fullName == null || fullName.isEmpty()) return null;

        String[] names = fullName.split(" ");
        String firstName = "", lastName = "";

        boolean first = true;
        for (String n: names){
            if (first)
            {
                first = !first;
                firstName = n;
                continue;
            }
            lastName += n + "";
        }
        return new Merchant(CPR,firstName,lastName);

    }

}

