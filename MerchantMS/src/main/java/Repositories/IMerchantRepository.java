/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor:
 */
package Repositories;

import MerchantCore.Merchant;

/**
 * Interface for merchant repository
 *
 * @author Khaled Wafic Khalil
 */
public interface IMerchantRepository {

        /**
         * Inserts merchant to the repository
         *
         * @param m merchant to be inserted
         * @return
         */
        boolean add(Merchant m);

        /**
         * Removes merchant from the repository
         *
         * @param m merchant to be removed
         * @return
         */
        boolean remove(Merchant m);

        /**
         * Retrieves merchant from the repository
         *
         * @param ID ID of the merchant to be retrieved
         * @return retrieved merchant
         */
        Merchant getMerchantByID(String ID);

}
