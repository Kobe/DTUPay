/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor: Mathias Bramming
 */
package MerchantCore;

import Repositories.IMerchantRepository;
import MessageAdapter.IMessageSender;
import RESTAdapter.DTO.TransferRequestDTO;
import Repositories.MerchantRepository;

/**
 * Core logic of the MerchantMS microservice
 *
 * @author Khaled Wafic Khalil  and Mathias Bramming
 */
public class MerchantManager {
    /**
     * Used for sending messages to other microservices
     */
    private IMessageSender messageSender;

    /**
     * Merchant repository
     */
    public IMerchantRepository repo;

    /**
     * Constructor  of the Merchant manager
     *
     * @param repo repository used in MerchantManager
     * @param messageSender message adapter used to communicate between services
     */
    public MerchantManager(MerchantRepository repo, IMessageSender messageSender) {
        this.repo = repo;
        this.messageSender = messageSender;
    }

    /**
     * Calls payment transfer between customer and merchant
     *
     * @param dto data transfer object for the transfer
     * @return true if transfer was successful
     */
    public boolean transfer(TransferRequestDTO dto) {
        if (repo.getMerchantByID(dto.getMerchantID()) == null) {
            System.out.printf("Error: No merchant with id '" + dto.getMerchantID() + "' found.");
            return false;
        }
        try {
            // Check if token is not used before
            String userCPR = messageSender.callCheckToken(dto.getTokenID());
            boolean tokenValid = !userCPR.equals("");
            if (tokenValid) {
                System.out.println("token valid");
                boolean paymentSuccess = messageSender
                        .callRequestPayment(dto.getAmount(), userCPR, dto.getMerchantID(), dto.getMessage(), dto.getTokenID());
                System.out.println("Payment succeded: " + paymentSuccess);
                return paymentSuccess;
            } else {
                System.out.println("token invalid");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
