/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor:
 */
package MerchantCore;

/**
 * Merchant class of the MerchantMS microservice
 *
 * @author Khaled Wafic Khalil
 *
 */
public class Merchant {

    /**
     * ID of the merchant
     */
    private String ID;
    /**
     * First name of the merchant
     */
    private String firstName;
    /**
     * Last name of the merchant
     */
    private String lastName;

    /**
     * Default constructor
     */
    public Merchant(){}

    /**
     * Merchant constructor
     *
     * @param ID id of the merchant
     * @param firstName first name of the merchant
     * @param lastName last name of the merchant
     */
    public Merchant(String ID, String firstName, String lastName){
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     *
     * @return
     */
    public String getFullName()
    {
        return this.firstName + " " + this.lastName;
    }

    /**
     *
     * @return
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     *
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName Last name of the merchant
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return
     */
    public String getID() {
        return this.ID;
    }

    /**
     *
     * @param firstName First name of the merchant
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @param ID ID of the merchant
     */
    public void setID(String ID) {
        this.ID = ID;
    }
}
