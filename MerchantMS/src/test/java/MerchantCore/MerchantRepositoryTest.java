/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor:
 */
package MerchantCore;

import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class MerchantRepositoryTest {

    @Test
    public void add() {
        HashMap<String, String> _merchants = new HashMap<>();
        String id = "010704-1212";
        String firstName = "Khaled";
        String lastName = "Khalil";

        Merchant m = new Merchant(id,firstName,lastName);
        _merchants.put(id, firstName);

        assertEquals(m.getFirstName(), _merchants.get(id));
    }

    @Test
    public void remove() {
        HashMap<String, String> _merchants = new HashMap<>();
        String id = "010704-1212";
        String firstName = "Khaled";
        String lastName = "Khalil";

        Merchant m = new Merchant(id,firstName,lastName);
        _merchants.put(id, firstName);
        assertEquals(m.getFirstName(), _merchants.get(id));

        _merchants.remove(id);
        assertEquals(null, _merchants.get(id));
    }

    @Test
    public void getMerchantByID() {

        HashMap<String, String> _merchants = new HashMap<>();
        String id = "010704-1212";
        String firstName = "Khaled";
        String lastName = "Khalil";
        _merchants.put(id, firstName +" "+ lastName);


        String name = _merchants.get(id);
        String[] names = name.split(" ");

        String firstName1 = "", lastName1 = "";
        boolean first = true;
        for (String n: names){
            if (first)
            {
                first = !first;
                firstName1 = n;
                continue;
            }
            lastName1 += n;
        }

        Merchant m = new Merchant(id, firstName1, lastName1);
        assertEquals(firstName, m.getFirstName());
        assertEquals(lastName, m.getLastName());
        assertEquals(id, m.getID());
    }
}