/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor:
 */
package CucumberTests.CustomerReportsTests;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CustomerReportsSteps {
    @Given("^A populated database$")
    public void a_populated_database() throws Throwable {
        // Write code here that turns the phrase above into concrete actions

    }

    @Given("^a specified date from (\\d+)\\.(\\d+)\\.(\\d+) until (\\d+)\\.(\\d+)\\.(\\d+)$")
    public void a_specified_date_from_until(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^Customer requests reports$")
    public void customer_requests_reports() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^Customer receives reports from (\\d+)\\.(\\d+)\\.(\\d+) until (\\d+)\\.(\\d+)\\.(\\d+)$")
    public void customer_receives_reports_from_until(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
