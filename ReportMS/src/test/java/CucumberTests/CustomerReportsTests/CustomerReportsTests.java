/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor:
 */
package CucumberTests.CustomerReportsTests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "Features",
 tags = {"@CucumberTests.CustomerReportsTests"})
public class CustomerReportsTests {
}
