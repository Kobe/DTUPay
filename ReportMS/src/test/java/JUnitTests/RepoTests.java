/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor:
 */
package JUnitTests;

import IReportsRepo.ReportsRepo;
import ReportCore.CustomerReport;
import ReportCore.MerchantReport;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class RepoTests {


    @Test
    public void insertCustomerReportTest(){
        ReportsRepo reportsRepo = new ReportsRepo();
        CustomerReport customerReport = new CustomerReport(new GregorianCalendar(2018, Calendar.NOVEMBER, 11).getTime().toString(), "Fast money", "123456789", 50, "0123456789");
        ArrayList<CustomerReport> customerReports = new ArrayList<CustomerReport>();
        customerReports.add(customerReport);
        assertEquals(true, reportsRepo.insertCustomerReport(new GregorianCalendar(2018, Calendar.NOVEMBER, 11).getTime(),
                new GregorianCalendar(2018, Calendar.NOVEMBER, 17).getTime(),
                customerReports, "1234"));
        //assertEquals(customerReport, reportsRepo.getAllCustomerReport("1234"));
    }


    @Test
    public void insertCustomerReportTestDate(){
        ReportsRepo reportsRepo = new ReportsRepo();
        CustomerReport customerReport = new CustomerReport(new GregorianCalendar(2018, Calendar.NOVEMBER, 11).getTime().toString(), "Fast money", "123456789", 50, "1234");
        ArrayList<CustomerReport> customerReports = new ArrayList<CustomerReport>();
        reportsRepo.insertCustomerReport(new GregorianCalendar(2018, Calendar.NOVEMBER, 11).getTime(),
                new GregorianCalendar(2018, Calendar.NOVEMBER, 17).getTime(),
                customerReports, "1234");

        //same query
        assertEquals(customerReport, reportsRepo.getCustomerReport(new GregorianCalendar(2018, Calendar.NOVEMBER, 11).getTime(),
                new GregorianCalendar(2018, Calendar.NOVEMBER, 17).getTime(),
                "1234").get(0));

        //different query
        assertEquals(null, reportsRepo.getCustomerReport(new GregorianCalendar(2018, Calendar.NOVEMBER, 11).getTime(),
                new GregorianCalendar(2018, Calendar.NOVEMBER, 21).getTime(),
                "1234"));

        //not existing users
        assertEquals(null, reportsRepo.getCustomerReport(new GregorianCalendar(2018, Calendar.NOVEMBER, 11).getTime(),
                new GregorianCalendar(2018, Calendar.NOVEMBER, 21).getTime(),
                "123"));
    }

    @Test
    public void insertMerchantReportTest(){
        ReportsRepo reportsRepo = new ReportsRepo();
        MerchantReport merchantReport = new MerchantReport(new GregorianCalendar(2018, Calendar.NOVEMBER, 11).getTime().toString(), "Fast money", "123456789", 50);
        ArrayList<MerchantReport> merchantReports = new ArrayList<MerchantReport>();
        assertEquals(true, reportsRepo.insertMerchantReport(new GregorianCalendar(2018, Calendar.NOVEMBER, 11).getTime(),
                new GregorianCalendar(2018, Calendar.NOVEMBER, 17).getTime(),
                merchantReports, "1234"));

    }


    @Test
    public void insertMerchantReportTestDate(){
        ReportsRepo reportsRepo = new ReportsRepo();
        MerchantReport merchantReport = new MerchantReport(new GregorianCalendar(2018, Calendar.NOVEMBER, 11).getTime().toString(), "Fast money", "123456789", 50);
        ArrayList<MerchantReport> merchantReports = new ArrayList<MerchantReport>();
        reportsRepo.insertMerchantReport(new GregorianCalendar(2018, Calendar.NOVEMBER, 11).getTime(),
                new GregorianCalendar(2018, Calendar.NOVEMBER, 17).getTime(),
                merchantReports, "1234");

        //same query
        assertEquals(merchantReport, reportsRepo.getMerchantReport(new GregorianCalendar(2018, Calendar.NOVEMBER, 11).getTime(),
                new GregorianCalendar(2018, Calendar.NOVEMBER, 17).getTime(),
                "1234").get(0));

        //different query
        assertEquals(null, reportsRepo.getMerchantReport(new GregorianCalendar(2018, Calendar.NOVEMBER, 11).getTime(),
                new GregorianCalendar(2018, Calendar.NOVEMBER, 21).getTime(),
                "1234"));

        //not existing users
        assertEquals(null, reportsRepo.getMerchantReport(new GregorianCalendar(2018, Calendar.NOVEMBER, 11).getTime(),
                new GregorianCalendar(2018, Calendar.NOVEMBER, 21).getTime(),
                "123"));
    }
}
