/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor:
 */
package IPaymentServiceReports;

import java.io.IOException;
import java.util.Date;

/**
 * Created by karol on 20.1.2019.
 */
public interface IPaymentServiceReports {
    public void getReportsMerchant(Date from, Date until, String merchantID) throws IOException, InterruptedException;
    public void getReportsCustomer(Date from, Date until, String CustomerID) throws IOException, InterruptedException;
}
