/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor:
 */
package ReportCore;

import IReportsRepo.IReportsRepo;
import MessageAdapter.IMessageSender;
import gherkin.lexer.Da;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class ReportsManager{
    private IMessageSender messageSender;
    public IReportsRepo repo;

    public ReportsManager(IReportsRepo repo, IMessageSender messageSender){
        this.repo = repo;
        this.messageSender = messageSender;
    }

    public ArrayList<CustomerReport> requestCustomerReports(Date from, Date until, String customerID) throws IOException, InterruptedException {
        return messageSender.callGetCustomerReports(from, until, customerID);
    }

    public ArrayList<MerchantReport> requestMerchantReports(Date from, Date until, String merchantID) throws IOException, InterruptedException {
        return messageSender.callGetMerchantReports(from, until, merchantID);
    }
}
