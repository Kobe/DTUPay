/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor:
 */
package ReportCore;

import IPaymentServiceReports.IPaymentServiceReports;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;

/**
 * Created by karol on 20.1.2019.
 */
public class PaymentServiceReportsQueue implements IPaymentServiceReports {
    private final static String QUEUE_NAME = "paymentServiceReports";
    private ConnectionFactory connectionFactory;
    private Connection connection;
    private Channel channel;

    public PaymentServiceReportsQueue() {
        this.connectionFactory = new ConnectionFactory();
        this.connectionFactory.setHost("rabbitmq");
        try {
            this.connection = this.connectionFactory.newConnection();
            this.channel = connection.createChannel();
            this.channel.queueDeclare(this.QUEUE_NAME, false, false, false, null);
            this.channel.queuePurge(this.QUEUE_NAME);
            this.channel.basicQos(1);

        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getReportsMerchant(Date from, Date until, String merchantID) throws IOException, InterruptedException {
        final String corrId = UUID.randomUUID().toString();
        DateFormat formatter = new SimpleDateFormat("MM/dd/yy");

        String replyQueueName = this.channel.queueDeclare().getQueue();
        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();



        JSONObject jsonObject = new JSONObject()
                .put("from", formatter.format(from))
                .put("until", formatter.format(until))
                .put("merchantID", merchantID);

        String message = jsonObject.toString();
        channel.basicPublish("", this.QUEUE_NAME, props, message.getBytes("UTF-8"));

        final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);

        String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                response.offer(new String(delivery.getBody(), "UTF-8"));
            }
        }, consumerTag -> {
        });

        String result = response.take();
        System.out.println(result);
        this.channel.basicCancel(ctag);
        /*
            TODO Parse the response and return the result
         */
    }

    @Override
    public void getReportsCustomer(Date from, Date until, String customerID) throws IOException, InterruptedException {
        final String corrId = UUID.randomUUID().toString();
        DateFormat formatter = new SimpleDateFormat("MM/dd/yy");

        String replyQueueName = this.channel.queueDeclare().getQueue();
        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();



        JSONObject jsonObject = new JSONObject()
                .put("from", formatter.format(from))
                .put("until", formatter.format(until))
                .put("customerID", customerID);

        String message = jsonObject.toString();
        channel.basicPublish("", this.QUEUE_NAME, props, message.getBytes("UTF-8"));

        final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);

        String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                response.offer(new String(delivery.getBody(), "UTF-8"));
            }
        }, consumerTag -> {
        });

        String result = response.take();
        System.out.println(result);
        this.channel.basicCancel(ctag);
        /*
            TODO Parse the response and return the result
         */
    }
}
