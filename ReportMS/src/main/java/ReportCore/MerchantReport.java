/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor:
 */
package ReportCore;

import java.util.Date;

/**
 * Created by karol on 19.1.2019.
 */
public class MerchantReport {
    private String date;
    private String note;
    private String transactionID;
    private int amount;

    public MerchantReport(String date, String note, String transactionID, int amount) {
        this.date = date;
        this.note = note;
        this.transactionID = transactionID;
        this.amount = amount;
    }

    public String  getDate() {
        return date;
    }

    public void setDate(String  date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
