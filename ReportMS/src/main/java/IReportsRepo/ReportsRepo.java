/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor:
 */
package IReportsRepo;

import ReportCore.CustomerReport;
import ReportCore.MerchantReport;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

public class ReportsRepo implements IReportsRepo{
    class QueryDates{
        Date from;
        Date until;

        public QueryDates(Date from, Date until){
            this.from = from;
            this.until = until;
        }
    }
    private HashMap<String, ArrayList<CustomerReport>> _CustomerRepo;
    private HashMap<String, ArrayList<MerchantReport>> _MerchantRepo;
    private HashMap<String, QueryDates> _lastCustomerQuery;
    private HashMap<String, QueryDates> _lastMerchantQuery;

    public ReportsRepo (){
        this._CustomerRepo = new HashMap<String, ArrayList<CustomerReport>>();
        this._MerchantRepo = new HashMap<String, ArrayList<MerchantReport>>();
        this._lastCustomerQuery = new HashMap<String, QueryDates>();
        this._lastMerchantQuery = new HashMap<String, QueryDates>();
    }

    @Override
    public boolean insertCustomerReport(Date from, Date until, ArrayList<CustomerReport> customerReport, String customerID) {
        this._CustomerRepo.put(customerID, customerReport);
        this._lastCustomerQuery.put(customerID, new QueryDates(from, until));

        return true;
    }

    @Override
    public boolean insertMerchantReport(Date from, Date until, ArrayList<MerchantReport> merchantReport, String merchantID) {
        // if the merchant has not requested reports before.
        this._MerchantRepo.put(merchantID, merchantReport);
        this._lastMerchantQuery.put(merchantID, new QueryDates(from, until));

        return true;
    }

    @Override
    public ArrayList<CustomerReport> getCustomerReport(Date from, Date until, String customerID) {
        if (!this._CustomerRepo.containsKey(customerID)){
            return null;
        }
        ArrayList<CustomerReport> customerReports = this._CustomerRepo.get(customerID);
        QueryDates lastQuery = this._lastCustomerQuery.get(customerID);

        if (lastQuery.from.compareTo(from) == 0 && lastQuery.until.compareTo(until) == 0){
            return customerReports;
        }

        return null;
    }



    @Override
    public ArrayList<MerchantReport> getMerchantReport(Date from, Date until, String merchantID) {
        if (!this._MerchantRepo.containsKey(merchantID)){
            return null;
        }

        ArrayList<MerchantReport> merchantReport = this._MerchantRepo.get(merchantID);
        QueryDates lastMerchantQuery = this._lastMerchantQuery.get(merchantID);

        if (lastMerchantQuery.from.compareTo(from) ==0 && lastMerchantQuery.until.compareTo(until) == 0){
            return merchantReport;
        }

        return null;
    }
}
