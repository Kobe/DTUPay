/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor:
 */
package IReportsRepo;

import ReportCore.CustomerReport;
import ReportCore.MerchantReport;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;

public interface IReportsRepo {
    public boolean insertCustomerReport(Date from, Date until, ArrayList<CustomerReport> customerReport, String customerID);
    public boolean insertMerchantReport(Date from, Date until, ArrayList<MerchantReport> merchantReport, String merchantID);
    public ArrayList<CustomerReport> getCustomerReport(Date from, Date until, String customerID);
    public ArrayList<MerchantReport> getMerchantReport(Date from, Date until, String merchantID);
}
