/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor: Mathias Bramming
 */
package RESTAdapter;

import IReportsRepo.ReportsRepo;
import MessageAdapter.MessageSender;
import ReportCore.ReportsManager;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeoutException;

@ApplicationPath("/")
public class RestApplication extends Application {
    private ReportsManager reportsManager = null;

    public RestApplication() throws IOException, TimeoutException {
        System.out.println("Initializing application...");
        reportsManager = new ReportsManager(new ReportsRepo(), new MessageSender());
    }

    @Override
    public Set<Object> getSingletons() {
        Set<Object> set = new HashSet<>();
        set.add(new StatusResource());
        set.add(new ReportResource(reportsManager));
        return set;
    }
}
