/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor:
 */
package RESTAdapter;

import IPaymentServiceReports.IPaymentServiceReports;
import ReportCore.CustomerReport;
import ReportCore.MerchantReport;
import ReportCore.PaymentServiceReportsQueue;
import ReportCore.ReportsManager;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by karol on 19.1.2019.
 */
@Path("/reports")
public class ReportResource {
    private ReportsManager reportsManager = null;

    public ReportResource(ReportsManager reportsManager){
        this.reportsManager = reportsManager;
    }

    @Path("/merchant/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMerchantReports(@PathParam("id") String merchantID, @QueryParam("from")String from, @QueryParam("until") String until){
        DateFormat formatter = new SimpleDateFormat("MM/dd/yy");

        try {
            Date fromDate = formatter.parse(from);
            Date untilDate = formatter.parse(until);

            ArrayList<MerchantReport> reports = new ArrayList<MerchantReport>();
            //TODO - create a message qeue to request reports from payment microservice
            reports = reportsManager.requestMerchantReports(fromDate, untilDate, merchantID);


            return Response.status(200).entity(new Gson().toJson(reports)).build();
        } catch (ParseException e) {
            e.printStackTrace();
            return Response.status(422).entity(new Gson().toJson("Wrong date format")).build();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return Response.status(500).build();
        } catch (IOException e) {
            e.printStackTrace();
            return Response.status(500).build();
        }
    }

    @Path("/customer/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerReports(@PathParam("id") String customerID, @QueryParam("from")String from, @QueryParam("until") String until){
        DateFormat formatter = new SimpleDateFormat("MM/dd/yy");
        try {
            Date fromDate = formatter.parse(from);
            Date untilDate = formatter.parse(until);

            ArrayList<CustomerReport> reports = new ArrayList<CustomerReport>();
            //TODO - create a message qeue to request reports from payment microservice
            reports = reportsManager.requestCustomerReports(fromDate, untilDate, customerID);
            return Response.status(200).entity(new Gson().toJson(reports)).build();
        } catch (ParseException e) {
            e.printStackTrace();
            return Response.status(422).entity(new Gson().toJson("Wrong date format")).build();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return Response.status(500).build();
        } catch (IOException e) {
            e.printStackTrace();
            return Response.status(500).build();
        }
    }
}
