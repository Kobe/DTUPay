/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor: Mathias Bramming
 */
package MessageAdapter;

import MessageAdapter.IMessageSender;
import ReportCore.CustomerReport;
import ReportCore.MerchantReport;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;

public class MessageSender implements IMessageSender {
    private Connection connection;
    private Channel channel;

    private String QUEUE_GET_CUSTOMER_REPORTS = "getCustomerReports";
    private String QUEUE_GET_MERCHANT_REPORTS = "getMerchantReports";

    public MessageSender() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        connection = factory.newConnection();
        channel = connection.createChannel();
    }


    private String call(String queueName, String message) throws IOException, InterruptedException {
        final String corrId = UUID.randomUUID().toString();
        System.out.println("Sending request: " + message);

        String replyQueueName = channel.queueDeclare().getQueue();
        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();

        channel.basicPublish("", queueName, props, message.getBytes("UTF-8"));

        final BlockingQueue<String> response = new ArrayBlockingQueue<String>(1);

        String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                response.offer(new String(delivery.getBody(), "UTF-8"));
            }
        }, consumerTag -> {
        });

        String result = response.take();
        System.out.println("Received message: " + result);
        channel.basicCancel(ctag);

        return result;
    }

    @Override
    public ArrayList<CustomerReport> callGetCustomerReports(Date from, Date until, String customerID) throws IOException, InterruptedException {
        DateFormat formatter = new SimpleDateFormat("MM/dd/yy");

        JSONObject jsonObject = new JSONObject()
                .put("from", formatter.format(from))
                .put("until", formatter.format(until))
                .put("customerID", customerID);
        String response = call(QUEUE_GET_CUSTOMER_REPORTS, jsonObject.toString());

        JSONArray jsonArray = new JSONArray(response);


        ArrayList<CustomerReport> customerReports = new ArrayList<CustomerReport>();

        for (int i = 0; i < jsonArray.length(); i++){
            JSONObject jObj = (JSONObject) jsonArray.get(i);
            String date = jObj.getString("date");
            String merchant = jObj.getString("to");
            int amount = jObj.getInt("amount");
            String note = jObj.getString("description");

            CustomerReport customerReport = new CustomerReport(date, note, "", amount, merchant);

            customerReports.add(customerReport);
        }

        return customerReports;
    }

    @Override
    public ArrayList<MerchantReport> callGetMerchantReports(Date from, Date until, String merchantID) throws IOException, InterruptedException {
        DateFormat formatter = new SimpleDateFormat("MM/dd/yy");

        JSONObject jsonObject = new JSONObject()
                .put("from", formatter.format(from))
                .put("until", formatter.format(until))
                .put("merchantID", merchantID);
        String response = call(QUEUE_GET_MERCHANT_REPORTS, jsonObject.toString());


        JSONArray jsonArray = new JSONArray(response);


        ArrayList<MerchantReport> merchantReports = new ArrayList<MerchantReport>();

        for (int i = 0; i < jsonArray.length(); i++){
            JSONObject jObj = (JSONObject) jsonArray.get(i);
            String date = jObj.getString("date");
            String merchant = jObj.getString("to");
            int amount = jObj.getInt("amount");
            String note = jObj.getString("description");

            MerchantReport merchantReport = new MerchantReport(date, note, "", amount);

            merchantReports.add(merchantReport);
        }

        return merchantReports;

    }
}
