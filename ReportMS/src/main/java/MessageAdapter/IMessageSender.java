/*
 **  Primary Contributor:   Karol Marso
 **  Secondary Contributor:
 */
package MessageAdapter;

import ReportCore.CustomerReport;
import ReportCore.MerchantReport;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public interface IMessageSender {
    public ArrayList<CustomerReport> callGetCustomerReports(Date from, Date until, String customerID) throws IOException, InterruptedException;
    public ArrayList<MerchantReport> callGetMerchantReports(Date from, Date until, String merchantID) throws IOException, InterruptedException;
}
