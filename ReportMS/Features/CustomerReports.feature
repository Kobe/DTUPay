#   Primary contributor:        Karol Marso
#   Secondary contributor(s):

Feature: Customer reports
  @CucumberTests.CustomerReportsTests
  Scenario: Customer wants to see reports
    Given A populated database
    And a specified date from 19.1.2019 until 26.1.2019
    When Customer requests reports
    Then Customer receives reports from 19.1.2019 until 26.1.2019