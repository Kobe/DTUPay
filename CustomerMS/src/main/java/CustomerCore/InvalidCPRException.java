/*
 **  Primary Contributor:   Bilal El Ghoul
 **  Secondary Contributor:
 */
package CustomerCore;

/**
 * Invalid CPR exception extends exception. Is used when wrong CPR is provided
 * @author Bilal El Ghoul
 */
public class InvalidCPRException extends Throwable {
    /**
     *Constructor of the exception
     *
     * @param message What went wrong with CPR
     */
    public InvalidCPRException(String message) {
        super(message);
    }
}
