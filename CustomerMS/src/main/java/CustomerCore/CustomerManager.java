/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor:
 */
package CustomerCore;

import ICustomerRepositories.ICustomerRepository;

/**
 *
 * Core logic for customer microservice
 *
 * @author Khaled Wafic Khalil
 */
public class CustomerManager {

    /**
     * Connector to database of the customer
     */
    public ICustomerRepository repo;

    /**
     * Constructor for customer manager
     * @param repo repository connector
     */
    public CustomerManager(CustomerRepository repo) {
        System.out.println("Instantiating repo");
        this.repo = repo;
    }

    /**
     * Registers customer
     *
     * @param customer customer to be registered
     * @return registered customer
     */
    public boolean RegisterCustomer(Customer customer)
    {
        return repo.add(customer);
    }

    public Customer RetreiveCustomer(String cpr) throws InvalidCPRException {
        return repo.getCustomerByCPR(cpr);
    }

}
