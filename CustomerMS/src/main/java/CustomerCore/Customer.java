/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor: Bilal El Ghoul
 */
package CustomerCore;

/**
 *
 * @Author Khaled Wafic Khalil and Bilal El Ghoul
 *
 */

public class Customer {

    /**
     * First name of the customer
     */
    private String firstName;
    /**
     * Last name of the customer
     */
    private String lastName;
    /**
     * CPR of the customer used for identification throughout the application
     */
    private String CPR;

    /**
     * Basic constructor
     */
    public Customer(){
        System.out.println("Customer init...");
    };

    /**
     * Constructor
     *
     * @param firstName
     * @param lastName
     * @param CPR must be 10 numbers
     * @throws InvalidCPRException is thrown when the user wrote wrong CPR (e.g. less than 10 numbers)
     */
    public Customer(String firstName, String lastName,String CPR) throws InvalidCPRException {
        this.firstName = firstName;
        this.lastName = lastName;
        this.CPR = CPR;

        /*if(!isCPRValid(CPR)) {
            throw new InvalidCPRException("Invalid CPR Format: CPR Must be 10 Digits Long");
        }else {
            this.CPR = CPR;
        }*/
    }

    /**
     *
     * @return returns Name in form of FirstName LastName
     */
    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    /**
     *
     * @return returns last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName last name of the customer
     */
    public void setLastName(String lastName) {
        System.out.println("Setting last name...");
        this.lastName = lastName;
    }

    /**
     *
     * @return first name of the customer
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @return CPR of the customer
     */
    public String getCPR() {
        return CPR;
    }

    /**
     *  sets the first name of the customer
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        System.out.println("Setting first name...");
        this.firstName = firstName;
    }

    /**
     * sets the CPR of the customer
     *
     * @param CPR
     */
    public void setCPR(String CPR){
        System.out.println("Setting CPR...");
        this.CPR = CPR;
    }

    /**
     * Checks for validity of the CPR
     *
     * @return true if the CPR is valid else false
     */
    public boolean isCPRValid() {

        // Not a real CPR validator, because it is hard to test with real CPR number.
        // This will ensure that the CPR is comprised of 10 digits.

        if(this.CPR.matches("\\d{10}")) {
            return true;
        }
        return false;
    }
}
