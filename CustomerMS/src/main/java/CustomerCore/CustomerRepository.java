/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor: Bilal El Ghoul
 */
package CustomerCore;

import ICustomerRepositories.ICustomerRepository;

import java.util.HashMap;

/**
 * @author Khaled Wafic Khalil and Bilal El Ghoul
 */
public class CustomerRepository implements ICustomerRepository {

    /**
     * Repository of all customers in form of CPR-customer name
     */
    private HashMap<String, String> _customers;

    /**
     * Constructor for the repository
     */
    public CustomerRepository(){
        _customers = new HashMap<String, String>();
    }

    /**
     * Adds customer to the repository. If customer already exists (CPR is already there) customer is not inserted
     *
     * @param c customer to be inserted
     * @return returns true if insertion of customer was successful
     */
    @Override
    public boolean add(Customer c) {

        if(_customers.containsKey(c.getCPR())) {
            return false;
        }
        _customers.put(c.getCPR(), c.getFullName());
        return true;
    }


    /**
     * Removes customer from the repository
     *
     * @param c customer to be removed
     * @return returns true if removal of the customer was successful
     */
    @Override
    public boolean remove(Customer c) {
        return _customers.remove(c.getCPR(), c.getFullName());

    }

    /**
     * Retrieves customer based on the CPR
     *
     * @param CPR CPR to be searched for
     * @return Customer object if the search was successful otherwise null
     * @throws InvalidCPRException throws invalid CPR exception in case wrong CPR was provided
     */
    @Override
    public Customer getCustomerByCPR(String CPR) throws InvalidCPRException {

        String name = _customers.get(CPR);

        if (name == null || name.isEmpty()) return null;

        String[] names = name.split(" ");
        String firstName = "", lastName = "";

        boolean first = true;
        for (String n: names){
            if (first)
            {
                first = !first;
                firstName = n;
                continue;
            }
            lastName += n + "";
        }
        return new Customer(firstName,lastName,CPR);
    }




}
