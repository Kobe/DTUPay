/*
 **  Primary Contributor:   Mathias Bramming
 **  Secondary Contributor: Karol Marso
 */
package MessageAdapter;

import CustomerCore.CustomerManager;
import CustomerCore.InvalidCPRException;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

/**
 * Message receiver for RabbitMQ
 *
 * @author Mathias Bramming and Karol Marso
 */
public class MessageReceiver {
    /**
     * Queue for checking of the customer
     */
    private String QUEUE_CHECK_CUSTOMER = "checkCustomer";
    /**
     * Customer manager instance - used for searching for a customer
     */
    private CustomerManager customerManager;
    /**
     * Channel used for communication
     */
    private Channel channel;


    /**
     * Constructor for the message receiver. Constructs message receiver and opens required connections and channels
     *
     * @param customerManager customer manager instance used in the service
     * @throws IOException
     * @throws TimeoutException
     */
    public MessageReceiver(CustomerManager customerManager) throws IOException, TimeoutException {
        this.customerManager = customerManager;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        Connection connection = factory.newConnection();
        this.channel = connection.createChannel();
    }

    /**
     * Intializes queues
     *
     * @throws IOException
     */
    public void initialize() throws IOException {
        initializeQueue(QUEUE_CHECK_CUSTOMER, this::callCheckCustomer);
    }


    /**
     * Initializes one queue
     *
     * @param queueName name of the queue to be initialized
     * @param func function to be berformed
     * @throws IOException
     */
    private void initializeQueue(String queueName, Function<String, String> func) throws IOException {
        channel.queueDeclare(queueName, false, false, false, null);
        System.out.println(" [*] Waiting for messages on queue '" + queueName + "'.");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "' on queue '" + queueName +"'");

            AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                    .Builder()
                    .correlationId(delivery.getProperties().getCorrelationId())
                    .build();

            String response = func.apply(message);
            channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, response.getBytes("UTF-8"));
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println("Sent reply for message received on queue '" + queueName + "'.");

        };
        channel.basicConsume(queueName, false, deliverCallback, consumerTag -> { });
    }

    /**
     * Checks customer for existence.
     *
     * @param CPR CPR of the customer
     * @return true if customer exists otherwise returns false
     */
    public String callCheckCustomer(String CPR) {
        // Parse message and return some response
        try {
            if(this.customerManager.RetreiveCustomer(CPR) != null){
                return "" + true;
            }

            return "" + false;
        } catch (InvalidCPRException e) {
            e.printStackTrace();
            return "" + false;
        }
    }
}
