/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor: Karol Marso
 */
package RESTAdapter;


import CustomerCore.*;
import com.google.gson.Gson;
import org.wildfly.swarm.spi.runtime.annotations.Post;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Main microservice fo CustomerMS
 *
 *
 * @author Khaled Wafic Khalil and Karol Marso
 *
 *
 */
@Path("/customer")
public class CustomerResource {

    /**
     * Customer manager used by the microservice
     */
    private CustomerManager customerManager;

    /**
     * Constructor of the microservice
     *
     * @param customerManager customer manager to be used in the microservice
     */
    public CustomerResource(CustomerManager customerManager){
        this.customerManager = customerManager;
    }

    /**
     * REST API call registerCustomer registers customer in the microservice
     *
     * @param customer customer to be registered
     * @return Returns 200 OK if the customer was registered successfully with JSONObject of the customer, 422 when the CPR was not valid and 400 if the customer could not be registered because of other reasons
     */
    @Path("/register")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerCustomer(Customer customer) {
        if(!customer.isCPRValid()) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity(new Gson().toJson("Invalid CPR")).build();
        }

        if (customerManager.RegisterCustomer(customer))
        {
            return Response.ok(new Gson().toJson(customer)).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(new Gson().toJson("Could not add the customer")).build();
    }

    /*@Path("/refund/{CPR}")
    @Post
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response refundCustomer(@PathParam("CPR") String CPR){

        return Response.status(200).build();
    }*/
}

