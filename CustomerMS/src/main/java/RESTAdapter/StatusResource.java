/*
 **  Primary Contributor:   Mathias Bramming
 **  Secondary Contributor:
 */
package RESTAdapter;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * Used for checking of the status of the service
 *
 * @author Mathias Bramming
 */
@Path("/status")
public class StatusResource {
    /**
     * Checks status of the microservice
     *
     * @return 200 OK Customer service is up
     */
    @GET
    @Produces("text/plain")
    public Response doGet() {
        return Response.ok("Customer service is up.").build();
    }
}