/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor:
 */
package ICustomerRepositories;

import CustomerCore.InvalidCPRException;
import CustomerCore.Customer;

/**
 * Interface for customer repository
 *
 * @author Khaled Wafic Khalil
 */
public interface ICustomerRepository {

    /**
     * Customer repository interface
     *
     * @param c customer to be added
     * @return true if the customer was successfully inserted
     */
    boolean add(Customer c);

    /**
     * Removes customer from the repository
     *
     * @param c customer to be removed
     * @return true if customer was successfully removed
     */
    boolean remove(Customer c);

    /**
     * Retrieves customer by CPR
     *
     * @param CPR CPR of the customer to be retrieved
     * @return Retrieved customer if exists else null
     * @throws InvalidCPRException if provided with incorrect CPR
     */
    Customer getCustomerByCPR(String CPR) throws InvalidCPRException;


}
