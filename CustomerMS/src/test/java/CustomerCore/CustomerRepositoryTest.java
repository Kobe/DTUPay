/*
 **  Primary Contributor:   Khaled Wafic Khalil
 **  Secondary Contributor:
 */
package CustomerCore;

import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class CustomerRepositoryTest {

    CustomerRepository repo = new CustomerRepository();

    @Test
    public void add() throws InvalidCPRException {
        String cpr = "010704-1212";
        String firstName = "Khaled";
        String lastName = "Khalil";

        Customer c = new Customer(firstName,lastName,cpr);
        repo.add(c);

        assertEquals(c.getFirstName(), repo.getCustomerByCPR(cpr).getFirstName());
    }

    @Test
    public void remove() throws InvalidCPRException {
        String cpr = "010704-1212";
        String firstName = "Khaled";
        String lastName = "Khalil";
        Customer c = new Customer(firstName,lastName,cpr);
        repo.add(c);
        repo.remove(c);

        assertNull(repo.getCustomerByCPR(cpr));
    }

    @Test
    public void getCustomerByCPR() throws InvalidCPRException {
        String cpr = "010704-1212";
        String firstName = "Khaled";
        String lastName = "Khalil";
        Customer c = new Customer(firstName,lastName,cpr);
        repo.add(c);

        Customer retreivedCustomer = repo.getCustomerByCPR(cpr);

        assertEquals(c.getFirstName(), retreivedCustomer.getFirstName());
        assertEquals(c.getLastName(), retreivedCustomer.getLastName());
        assertEquals(c.getCPR(), retreivedCustomer.getCPR());

    }
}